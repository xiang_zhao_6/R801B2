import yaml

import Datas


def gain_switch_server(file_name, func_name):
    with open(f"./Datas/type_in_{file_name}.yml", 'r', encoding='utf-8') as f:
        datas = yaml.load(f, Loader=yaml.FullLoader)[func_name]
    return list(datas.values())


def gain_write_groupOwner(file_name, func_name):
    with open(f"./Datas/type_in_{file_name}.yml", 'r', encoding='utf-8') as f:
        datas = yaml.load(f, Loader=yaml.FullLoader)[func_name]
    return list(datas.values())


def gain_write_RenameGroupOwner(file_name, func_name):
    with open(f"./Datas/type_in_{file_name}.yml", 'r', encoding='utf-8') as f:
        datas = yaml.load(f, Loader=yaml.FullLoader)[func_name]
    return list(datas.values())


def write_groupMember(file_name, func_name):
    with open(f"./Datas/type_in_{file_name}.yml", 'r', encoding='utf-8') as f:
        datas = yaml.load(f, Loader=yaml.FullLoader)[func_name]
    return list(datas.values())


if __name__ == '__main__':
    print(write_groupMember('member', 'member'))
