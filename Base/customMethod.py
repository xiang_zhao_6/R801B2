"""
    :account for: 封装自定义方法
"""

import time
from cgi import print_exception
from telnetlib import EC
from time import sleep
import allure
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import subprocess
from log.configLog import Logger
from selenium.webdriver.common.action_chains import ActionChains


class MethodDepot:
    def __init__(self, driver):
        self.driver = driver

    def get_element(self, solid, timeout=6, poll_frequency=1):
        """
        显示等待获取元素
        :param solid: 元素定位方式
        :param timeout: 等待超时时间
        :param poll_frequency: 轮询频率
        :return: 元素对象或None
        """
        wait = WebDriverWait(self.driver, timeout, poll_frequency)
        try:
            obj = wait.until(lambda x: x.find_element(*solid))
        except Exception:
            return None
        else:
            return obj

    def get_elements(self, solid, timeout=6, poll_frequency=1):
        """
        显示等待获取元素列表
        :param solid: 元素定位方式
        :param timeout: 等待超时时间
        :param poll_frequency: 轮询频率
        :return: 元素列表或None
        """
        wait = WebDriverWait(self.driver, timeout, poll_frequency)
        try:
            obj = wait.until(lambda x: x.find_elements(*solid))
        except Exception:
            return None
        else:
            return obj

    def hit(self, solid):
        """
        模拟点击事件
        :param solid: 形参
        :return: None
        """
        action = TouchAction(self.driver)
        if isinstance(solid, tuple):
            solid = self.get_element(solid)
        action.tap(solid)
        action.perform()

    def clear_away(self, solid):
        """
        清空文本框字符方法
        :param solid: 形参
        :return: None
        """
        self.get_element(solid).clear()

    def write_in(self, solid, contents):
        """
        模拟数据输入事件
        :param solid: 形参
        :param contents: 获取元素形参
        :return: None
        """
        if isinstance(solid, tuple):
            solid = self.get_element(solid)
        solid.clear()  # 清空文本框
        solid.send_keys(contents)

    def get_screen_size(self):
        """
        获取屏幕尺寸方法
        :return: None
        """
        measure = self.driver.get_window_size()
        width = measure['width']
        height = measure['height']
        return width, height

    def down_glide(self, t=0):
        """
        模拟屏幕下划动作
        :return: None
        """
        # width = self.get_screen_size()[0]
        # height = self.get_screen_size()[1]
        width, height = self.get_screen_size()
        self.driver.swipe(width * 0.5, height * 0.8, width * 0.5, height * 0.9)

    def long_press(self, ele, T=11000):
        """
        模拟长按操作
        :return: None
        """
        if isinstance(ele, tuple):
            ele = self.get_element(ele)
        action = ActionChains(self.driver)
        action.click_and_hold(ele).pause(T / 1000).release().perform()

    def up_glide(self, t=0):
        """
        模拟屏幕上划动作
        :param t: 划动过程等待时常
        :return: None
        """
        # width = self.get_screen_size()[0]
        # height = self.get_screen_size()[1]
        width, height = self.get_screen_size()
        self.driver.swipe(width * 0.5, height * 0.8, width * 0.5, height * 0.6, t)

    def judgment_page_single(self, nature):
        """
        判断页面唯一元素
        :param nature: 元素对象形参
        :return: Boolean
        """
        return bool(nature)

    def father_and_son_level(self, sire, son):
        """
        父子级定位方法
        :param sire: 父元素
        :param son: 子元素
        :return: Boolean
        """
        sire_element = self.get_element(sire)
        waits = WebDriverWait(sire_element, 6, 1)
        try:
            try_element = waits.until(lambda x: x.find_element(*son))
        except Exception:
            return False
        else:
            return try_element

    def recursion_element(self, solid):
        """
        利用递归方法 实现捕捉元素 up
        :param solid: 递归目标
        :return: None
        """
        objecttive = self.get_element(solid)
        if objecttive:
            return objecttive
        else:
            self.up_glide(2000)
            return self.recursion_element(solid)

    def recursion_element_down(self, solid):
        """
        利用递归方法 实现捕捉元素 down
        :param solid: 递归目标
        :return: None
        """
        objecttive = self.get_element(solid)
        if objecttive:
            return objecttive
        else:
            self.down_glide(2000)
            return self.recursion_element(solid)

    def judgment_toast(self, toast):
        """
        捕捉Toast元素
        :param toast: 目标元素
        :return: None
        """
        waits = WebDriverWait(self.driver, 10, 1)
        try:
            pageToast = waits.until(lambda x: x.find_element_by_xpath("//*[contains(@text, '{}')]".format(toast)))
        except Exception as e:
            return False
        else:
            return pageToast

    def judgment_enabled(self, solid):
        """
        判断按钮是否高亮or置灰状态
        :param solid: 元素对象
        :return: Boolean
        """
        enabled = solid.get_attribute("enabled")
        selected = solid.get_attribute("selected")
        return bool(enabled or selected)

    def judgment_checked(self, solid):
        """
        判断按钮是否被选中状态
        :param solid: 元素对象
        :return: Boolean
        """
        checked = solid.get_attribute("checked")
        return bool(checked)

    def touch_action1(self, x, y):
        """
        坐标方法长按事件
        :param x: X轴
        :param y: Y轴
        :return: None
        """
        action = TouchAction(self.driver)
        action.long_press(x=x, y=y).wait(30000).perform()

    def touch_action(self, x, y):
        """
        坐标方法点击事件
        :param x: X轴
        :param y: Y轴
        :return: None
        """
        action = TouchAction(self.driver)
        action.tap(x=x, y=y).perform()

    def issue_screenshot(self, driver, txt):
        """
        Allure截图方法
        :param driver: 驱动
        :param txt: 描述文案
        :return: None
        """
        time.sleep(2)
        if txt is not None:  allure.attach(driver.get_screenshot_as_png(), txt, allure.attachment_type.PNG)

    # 重写获取toast方法
    def find_toast(self, message, timeout, poll_frequency):
        """
        u'''获取toast信息文本并验证'''
        :param message: 元素
        :param timeout: 时间(单位:s)
        :param poll_frequency: 轮询频率
        :return: toast
        """
        message1 = "//*[contains(@text,\'{}\']".format(message)
        element = WebDriverWait(self.driver, timeout, poll_frequency).until(
            EC.presence_of_element_located((By.XPATH, message1)))
        return element.text

    def judge_boxContent(self):
        """
        查找页面文本框中的文本内容,判断文本内容是否为空
        :return: 文本框元素信息
        """
        text_boxes = self.driver.find_elements_by_xpath("//android.widget.EditText")
        for text_box in text_boxes:
            text = text_box.get_attribute("text")
            if text:
                print("文本框遍历内容：", text, '\n')
                return True
        return False
        # text_boxes = self.driver.find_elements_by_xpath("//android.widget.EditText")
        # return any(text_box.get_attribute("text") for text_box in text_boxes)
