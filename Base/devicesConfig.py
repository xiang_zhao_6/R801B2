"""
    :account for: 设备属性配置
"""
from appium import webdriver


def device_info():
    focus = dict()
    focus['platformName'] = 'Android'
    focus['platformVerstion'] = '10.0.1'
    focus['deviceName'] = 'HuaWei'
    focus["udid"] = "2FD0221C09010263"
    # focus["udid"] = "192.168.3.75:5555"
    # focus['skipServerInstallation'] = True
    # focus['skipDeviceInitialization'] = True
    #
    # '''801'''
    focus['appPackage'] = 'com.zbgd.r801b2'
    focus['appActivity'] = 'com.zbgd.jzapp.activity.login.LaunchActivity'

    focus['resetKeyboard'] = True
    focus['unicodeKeyboard'] = True
    focus['noReset'] = True
    focus['automationName'] = 'uiautomator2'
    return webdriver.Remote('http://localhost:4723/wd/hub', focus)
