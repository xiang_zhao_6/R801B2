from Base.devicesConfig import device_info
from Base.customMethod import MethodDepot
from .dataDriven import gain_switch_server
from Base.dataDriven import gain_write_groupOwner
from .dataDriven import write_groupMember
from .dataDriven import gain_write_RenameGroupOwner