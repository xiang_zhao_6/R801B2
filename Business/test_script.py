import inspect
import logging
import os
import subprocess
from datetime import datetime
from telnetlib import EC
from selenium.webdriver.support import expected_conditions as EC
import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

import Handle, datetime
import Base
import pytest, time
import requests
import allure_commons
from allure_commons import plugin_manager
from log.configLog import Logger, log_decorator


class Test_Script(object):
    def setup_class(self):
        # 实例化Logger类，并传入参数
        self.logger = Logger(logger_name='test_script', file_name='./log/log.log', log_level=logging.DEBUG)
        try:
            self.driver = Base.device_info()
            self.logger.log("初始化device", 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')
        try:
            self.handle = Handle.HandleTotal(self.driver)
            self.logger.log("初始化Handle", 'info')
            print('\n', "屏幕尺寸: ", self.driver.get_window_size())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 判断屏幕息屏
        if self.driver.is_locked():
            self.driver.unlock()
            self.logger.log('\n' + "判断屏幕息屏状态", 'info')

        # 清理 AllureReport目录数据
        dir_path = r'.\Report\xml'
        for filename in os.listdir(dir_path):
            file_path = os.path.join(dir_path, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    # 删除文件
                    os.unlink(file_path)
                    # self.logger.log("删除Report文件", 'info')
                elif os.path.isdir(file_path):
                    # 删除空文件夹
                    os.rmdir(file_path)
                    self.logger.log("删除空文件夹", 'info')
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
                self.logger.log("清理 AllureReport目录数据", 'error')
        time.sleep(2)

    # # @pytest.mark.skip(reason="跳过成员聊天室")(reason="跳过")
    # @pytest.mark.run(order=1)
    # @allure.epic('测试对象: 应用锁')
    @allure.tag('01')
    @allure.story('测试对象: 应用锁')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('应用锁界面,输入解锁密码')
    @allure.severity("blocker")
    # @allure.title('开屏广告-测试')
    # @allure.feature(' feature ')
    @pytest.mark.parametrize('info', Base.gain_switch_server('server', 'rule_ip'))
    @pytest.mark.parametrize('appLockPwd', Base.gain_switch_server('appLockPwd', 'lockPwd'))
    def test_index(self, info, appLockPwd):
        # 单击锁屏页_切换服务器
        try:
            self.handle.init_index_handle.click_index_page_element()
            self.logger.log("单击锁屏页_切换服务器", 'info')
            assert self.handle.init_index_handle.judgment_page_single(
                self.handle.init_index_handle.get_lock_screen_server()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 判断文本内容是否为空
        try:
            if self.handle.init_infor_handle.judge_boxContent == False:
                self.logger.log("判断文本内容是否为空", 'info')
                # 输入ip及端口
                self.handle.init_index_handle.write_ip(info['ip'])
                self.logger.log("如果ip为空,则输入服务器IP", 'info')
                self.handle.init_index_handle.write_port(info['port'])
                self.logger.log("输入服务器端口", 'info')

            if self.handle.init_index_handle.get_input_server().get_attribute('text') != "117.71.59.159":
                # 输入ip及端口
                self.handle.init_index_handle.write_ip(info['ip'])
                self.logger.log("如果非:117.71.59.159, 则输入服务器IP", 'info')
                self.handle.init_index_handle.write_port(info['port'])
                self.logger.log("输入服务器端口", 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 点击确认
        try:
            self.handle.init_index_handle.click_but()
            self.logger.log("输入服务器IP、服务器端口,点击保存", 'info')
            assert self.handle.init_index_handle.judgment_page_single(
                self.handle.init_index_handle.get_lock_screen_server())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 点击应用锁键盘
        try:
            self.handle.init_index_handle.cl_keypad(appLockPwd['pwd'])
            self.logger.log(f"输入应用锁密码", 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')
        time.sleep(2)
        assert self.handle.init_infor_handle.judgment_page_single(
            self.handle.init_infor_handle.get_assert_msgTitle())
        time.sleep(3)

    # @allure.epic('创建临时群聊')
    @allure.tag('02')
    # @pytest.mark.skip(reason="创建临时群聊")
    @allure.story('测试对象: 创建临时群聊')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('创建临时群聊')
    @allure.severity("normal")
    @pytest.mark.parametrize('info', Base.gain_write_groupOwner('groupOwner', 'group_owner'))
    @pytest.mark.parametrize('member', Base.write_groupMember('member', 'member'))
    def test_create_tempGroup(self, member, info):
        # 更多按钮
        try:
            self.handle.init_infor_handle.click_more_button()
            self.logger.log("更多按钮", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_create_group())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 更多_创建群聊
        try:
            self.handle.init_infor_handle.click_create_group()
            self.logger.log("更多_创建群聊", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_text())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 发起群聊页-临时群聊
        try:
            self.handle.init_infor_handle.click_group_temp_type()
            self.logger.log("发起群聊页-临时群聊", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_text_group())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 临时群聊-发起群聊-查询并选择成员
        try:
            self.handle.init_infor_handle.click_start_tempGrouopQueryMember(member['mem_name'])
            self.logger.log("临时群聊-发起群聊-查询并选择成员", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_group_temp_check())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 临时群聊-勾选群成员
        try:
            self.handle.init_infor_handle.click_group_temp_check()
            self.logger.log("临时群聊-进入勾选群成员列表", 'info')
            time.sleep(2)
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 临时群聊-发起群聊-查询成员-清空文本框
        try:
            self.handle.init_infor_handle.clear_away(self.handle.init_infor_handle.start_tempGrouopQueryMember)
            self.logger.log('临时群聊-发起群聊-查询成员-清空文本框', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 临时群聊,开始勾选成员
        try:
            self.handle.init_infor_handle.click_group_temp_check()
            self.logger.log('临时群聊,开始勾选成员', 'info')
            time.sleep(6)
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 临时群聊-发起群聊-下一步按钮 获取元素的enabled状态
        try:
            if self.handle.init_infor_handle.judgment_enabled(
                    self.handle.init_infor_handle.get_tempGroupChat_electMenber_next()):
                # 创建临时群聊_单击下一步按钮
                self.handle.init_infor_handle.click_create_group_step()
                self.logger.log('创建临时群聊_单击下一步按钮', 'info')
                assert self.handle.init_infor_handle.judgment_page_single(
                    self.handle.init_infor_handle.get_assert_text_group()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_输入群昵称
        try:
            self.handle.init_infor_handle.write_create_group_nick(info['group_nick'])
            self.logger.log('创建临时群聊_输入群昵称', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_title_createGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_单击开始选择群主
        try:
            self.handle.init_infor_handle.click_startElectGroupOwner()
            self.logger.log('创建临时群聊_单击开始选择群主', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_electGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_单击选择群主
        try:
            self.handle.init_infor_handle.click_elect_groupOwner()
            self.logger.log('创建临时群聊_单击选择群主', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_electGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_选择群主后_并保存
        try:
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('创建临时群聊_选择群主后_并保存', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_title_createGroup())
            time.sleep(1)
        except Exception as e:
            self.logger.log('\n' + '创建临时群聊_选择群主后_并保存', '')

        # 创建临时群聊界面_点击完成
        try:
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('创建临时群聊界面_点击完成', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_msgTitle())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    # @allure.epic('创建案件群聊')
    @allure.tag('03')
    # @pytest.mark.skip(reason="创建案件群聊")
    @allure.story('创建案件群聊')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('创建案件群聊')
    @allure.severity("normal")
    @pytest.mark.parametrize('info', Base.gain_write_groupOwner('groupOwner', 'group_owner'))
    @pytest.mark.parametrize('member', Base.write_groupMember('member', 'member'))
    def test_create_caseGroup(self, member, info):
        # 更多按钮
        try:
            self.handle.init_infor_handle.click_more_button()
            self.logger.log("更多按钮", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_create_group())
        except Exception as e:
            self.logger.log('\n' + "更多按钮", '')

        # 更多_创建群聊
        try:
            self.handle.init_infor_handle.click_create_group()
            self.logger.log("更多_创建群聊", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_text())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 选择案件群聊类型_案件群聊
        try:
            self.handle.init_infor_handle.click_case_group_chat()
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_text_group())
            self.logger.log("选择案件群聊类型_案件群聊", 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', '')

        # 案件群聊-发起群聊-查询并选择成员
        try:
            self.handle.init_infor_handle.click_start_tempGrouopQueryMember(member['mem_name'])
            self.logger.log("案件群聊-发起群聊-查询并选择成员", 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_group_temp_check())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 案件群聊-勾选群成员
        try:
            self.handle.init_infor_handle.click_group_temp_check()
            self.logger.log("案件群聊-进入勾选群成员列表", 'info')
            time.sleep(2)
        except Exception as e:
            raise self.logger.log('\n' + "案件群聊-勾选群成员", '')

        # 案件群聊-发起群聊-查询成员-清空文本框
        try:
            self.handle.init_infor_handle.clear_away(self.handle.init_infor_handle.start_tempGrouopQueryMember)
            self.logger.log('案件群聊-发起群聊-查询成员-清空文本框', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 案件群聊,开始勾选成员
        try:
            self.handle.init_infor_handle.click_group_temp_check()
            self.logger.log('案件群聊,开始勾选成员', 'info')
            time.sleep(6)
        except Exception as e:
            raise self.logger.log('\n' + '案件群聊,开始勾选成员', '')

        # 案件群聊-发起群聊-下一步按钮 获取元素的enabled状态
        try:
            if self.handle.init_infor_handle.judgment_enabled(
                    self.handle.init_infor_handle.get_tempGroupChat_electMenber_next()):
                # 创建案件群聊_单击下一步按钮
                self.handle.init_infor_handle.click_create_group_step()
                self.logger.log('创建案件群聊_单击下一步按钮', 'info')
                assert self.handle.init_infor_handle.judgment_page_single(
                    self.handle.init_infor_handle.get_assert_text_group()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建案件群聊_输入群昵称
        try:
            self.handle.init_infor_handle.write_create_fixedGroupNick("caseGroupChat")
            self.logger.log('创建案件群聊_输入群昵称', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_title_createGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_单击开始选择群主
        try:
            self.handle.init_infor_handle.click_startElectGroupOwner()
            self.logger.log('创建案件群聊_单击开始选择群主', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_electGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_单击选择群主
        try:
            self.handle.init_infor_handle.click_elect_groupOwner()
            self.logger.log('创建案件群聊_单击选择群主', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_electGroup())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊_选择群主后_并保存
        try:
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('创建案件群聊_选择群主后_并保存', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_title_createGroup())
            time.sleep(1)
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        # 创建临时群聊界面_点击完成
        try:
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('创建案件群聊界面_点击完成', 'info')
            assert self.handle.init_infor_handle.judgment_page_single(
                self.handle.init_infor_handle.get_assert_msgTitle())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('04')
    # @pytest.mark.skip(reason="跳过查询已创建临时群聊聊天界面")
    @allure.story('查询已创建临时群聊聊天界面')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('查询已创建群聊,查询并进入聊天界面操作..')
    @allure.severity("blocker")
    @pytest.mark.parametrize('info', Base.gain_write_groupOwner('groupOwner', 'group_owner'))
    @pytest.mark.parametrize('member', Base.write_groupMember('member', 'member'))
    def test_msgChat(self, info, member):
        try:
            self.handle.ini_msgChatHandle.writeIn_msgPage_query(info['group_nick'])
            self.logger.log('查询已创建群聊', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_group_info())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_group_info()
            self.logger.log('查询已创建群聊,进入聊天界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_page_cant())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            text = datetime.datetime.now();
            self.handle.ini_msgChatHandle.input_sendwWord("注意安全")
            time.sleep(1)
            self.handle.ini_msgChatHandle.click_chatPage_sendwWordBtn()
            self.logger.log('聊天界面,发送聊天内容', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chatPage_moreIcon())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_sendMsgContent())
            self.logger.log('\n' + '校验 已发送消息内容', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        time.sleep(0.5)
        try:
            self.handle.ini_msgChatHandle.click_sendvoice()
            self.logger.log('聊天界面,点击语音按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.asse_sendvoice())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_longPressToSpeak()
            self.logger.log('聊天界面,长按语音按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_message_audio())
            time.sleep(1)
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.logger.log('群聊界面-更多按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_btn_camera())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_btn_photo()
            self.logger.log('聊天界面-更多Toast: 相册', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_tvCheck())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_tvCheck()
            self.logger.log('聊天界面-更多Toast: 相册-勾选图片或视频', 'info')
            assert self.handle.ini_msgChatHandle.judgment_enabled(self.handle.ini_msgChatHandle.get_tvCheck())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_sele_photo()
            self.logger.log('聊天界面-更多Toast: 相册-勾选图片或视频,点击完成', 'info')
            time.sleep(1)
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_message_img_send())
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.handle.ini_msgChatHandle.click_btn_location()
            self.logger.log('聊天界面-更多-发送位置', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_message_img_send()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            time.sleep(3)
            self.handle.ini_msgChatHandle.click_map_send_locasion_ok()
            self.logger.log('聊天界面-更多-发送位置-确定', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_message_location_send())
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.handle.ini_msgChatHandle.write_btn_group_read()
            self.logger.log('聊天界面-更多-阅后即焚', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_iv_photo())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_iv_photo()
            self.logger.log('聊天界面-更多-阅后即焚-选择图片', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_tv_res())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_tv_res()
            self.logger.log('聊天界面-更多-阅后即焚-选择图片-从相册选取', 'info')
            assert self.handle.ini_msgChatHandle.judgment_enabled(self.handle.ini_msgChatHandle.get_tvCheck())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.handle.ini_msgChatHandle.click_tvCheck()
            self.logger.log('相册-勾选图片或视频', 'info')
            assert self.handle.ini_msgChatHandle.judgment_enabled(self.handle.ini_msgChatHandle.get_tvCheck())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_sele_photo()
            self.logger.log('相册-勾选图片或视频,点击完成', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_le_read_send()) \
                   or self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_le_video_send())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_bar_tvClose()
            self.logger.log('聊天界面-更多-阅后即焚-关闭', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chatPage_moreIcon())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.logger.log('群聊界面-更多按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_btn_camera())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_btn_group_share()
            self.logger.log('聊天界面-更多-设备分享', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_group_share_check())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_group_share_check()
            self.logger.log('聊天界面-更多-设备分享-设备复选框', 'info')
            assert self.handle.ini_msgChatHandle.judgment_enabled(
                self.handle.ini_msgChatHandle.get_group_share_btn_next())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.cli_group_share_btn_next()
            self.logger.log('聊天界面-更多-设备分享-设备复选框-分享', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_message_img_send())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_chatPage_moreIcon()
            self.logger.log('聊天界面-更多', 'info')
            self.handle.ini_msgChatHandle.click_btn_group_tts()
            self.logger.log('聊天界面-更多-应急语音', 'info')
            time.sleep(2)
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_btn_group_tts_view_edit())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.input_btn_group_tts_view_edit("注意安全")
            self.logger.log('聊天界面-更多-应急语音-文本输入框', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_btn_group_tts_view_edit_tv_ok())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.ini_msgChatHandle.click_btn_group_tts_view_edit_tv_ok()
            self.logger.log('聊天界面-更多-应急语音-文本输入框-确定', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(self.handle.ini_msgChatHandle.get_message_audio())
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.ini_msgChatHandle.get_chat_error()) == False
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('05')
    # @pytest.mark.skip(reason="跳过进入群聊设置界面")
    @allure.story('群聊设置')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入群聊设置界面..')
    @allure.severity("minor")
    @pytest.mark.parametrize('renameGroup', Base.gain_write_RenameGroupOwner('RenameGroupOwner', 'group_owner'))
    @pytest.mark.parametrize('renameGroupNotice', Base.gain_write_RenameGroupOwner('RenameGroupOwner', 'group_two'))
    @pytest.mark.parametrize('caseGroupNickName', Base.gain_write_RenameGroupOwner('groupOwner', 'group_owner1'))
    def test_msgChatSet(self, renameGroup, renameGroupNotice, caseGroupNickName):
        try:
            self.handle.init_msgChatSetHandle.click_msgChatSetBtn()
            self.logger.log('进入群聊设置界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_group_chat_set()) == False
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_msgChatSetHandle.click_groupChatSetPersonnelAdd()
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetAddMemberTitle())
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch()) == False
            self.logger.log('群聊设置界面-添加群成员', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            if self.handle.ini_msgChatHandle.judgment_page_single(
                    self.handle.init_msgChatSetHandle.get_groupChatSetMemberIcon()):
                self.handle.ini_msgChatHandle.cli_group_share_check()
                assert self.handle.ini_msgChatHandle.judgment_enabled(
                    self.handle.init_msgChatSetHandle.get_groupChatSetConfirmBtn())
                self.logger.log('群聊设置界面-添加群成员-勾选成员复选框', 'info')
            else:
                self.handle.ini_msgChatHandle.click_chatPageBackBtn()
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            if self.handle.ini_msgChatHandle.judgment_enabled(
                    self.handle.init_msgChatSetHandle.get_groupChatSetConfirmBtn()):
                self.handle.ini_msgChatHandle.cli_group_share_btn_next()
                assert self.handle.ini_msgChatHandle.judgment_page_single(
                    self.handle.init_msgChatSetHandle.get_groupChatSetAddMemberTitle()) == False
                assert self.handle.ini_msgChatHandle.judgment_page_single(
                    self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())
                self.logger.log('群聊设置界面-添加群成员', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_msgChatSetHandle.click_groupChatSetPersonnelLessen()
            self.logger.log('群聊设置界面-点击移除群成员按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetLessenMemberTitle())
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch()) == False
            self.logger.log('群聊设置界面-移除群成员', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            if self.handle.ini_msgChatHandle.judgment_page_single(
                    self.handle.init_msgChatSetHandle.get_groupChatSetMemberIcon()):
                self.handle.ini_msgChatHandle.cli_group_share_check()
                self.logger.log('群聊设置界面-移除群成员-勾选成员复选框', 'info')
            else:
                self.handle.ini_msgChatHandle.click_chatPageBackBtn()
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('群聊设置界面-移除群成员界面,点击确认', 'info')
            self.handle.ini_msgChatHandle.click_btn_group_tts_view_edit_tv_ok()
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetLessenMemberTitle()) == False
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())
            self.logger.log('群聊设置界面-移除群成员', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_msgChatSetHandle.click_groupChatSetName()
            self.logger.log('群聊设置界面-进入重命名群名称界面', 'info')
            time.sleep(1)
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetGroupRename())
            self.handle.init_msgChatSetHandle.input_groupChatSetRenameBox(renameGroup['group_nick'])
            self.logger.log('群聊设置界面-重命名群名界面-输入群名称', 'info')
            self.handle.init_infor_handle.click_electGroupOwner_save()
            self.logger.log('群聊设置界面-重命名群名界面-输入群名称,保存', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_msgChatSetHandle.click_groupChatSetNotice()
            self.logger.log('群聊设置界面-群公告,点击进入', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetInRenameNotice())
            self.logger.log('断言群公告Title', 'info')

            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch()) == False
            self.handle.init_infor_handle.reuse_click_createSave()
            self.logger.log('调用:保存临时群聊确认方法, 点击编辑按钮', 'info')

            self.handle.init_msgChatSetHandle.write_groupChatSetRenameNotice(renameGroupNotice['group_nick'])
            self.logger.log('输入群公告', 'info')

            self.handle.init_infor_handle.reuse_click_createSave()
            self.logger.log('调用:保存临时群聊确认方法, 输入群公告、点击完成', 'info')

            self.handle.ini_msgChatHandle.click_chatPageBackBtn()
            self.logger.log('调用:聊天界面-返回, 返回群聊设置界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetRenameNoticeAver())
            self.logger.log('断言群聊设置界面-编辑保存后的群公告', 'info')

            self.handle.init_msgChatSetHandle.click_msgChatSetBtn()
            self.logger.log('进入群聊设置界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_group_chat_set()) == False
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())

            self.handle.init_msgChatSetHandle.click_groupChatSetPermission()
            self.logger.log('进入权限设置界面', 'info')
            assert self.handle.init_msgChatSetHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetPermissionPageTitle())
            self.logger.log('断言权限设置Title', 'info')

            self.handle.init_msgChatSetHandle.click_groupChatSetPermissionPageChecked1()
            self.logger.log('群聊设置界面 - 权限设置界面点击: 一级话权单选框', 'info')

            self.handle.init_infor_handle.reuse_click_createSave()
            self.logger.log('调用:保存临时群聊确认方法, 权限设置、点击完成', 'info')

            self.handle.ini_msgChatHandle.click_chatPageBackBtn()
            self.logger.log('调用:聊天界面-返回, 点击返回权限设置界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetInRenameNotice())

            self.handle.init_msgChatSetHandle.click_groupChatSetUpTopSwitch()
            self.logger.log('群聊界面-点击置顶按钮', 'info')
            assert self.handle.ini_msgChatHandle.judgment_checked(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())

            self.handle.init_msgChatSetHandle.click_groupChatSetHistoryMsg()
            self.logger.log('群聊设置界面-进入查找聊天记录页面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_checked(
                self.handle.init_msgChatSetHandle.get_groupChatSetHistoryMsgTitle())

            self.handle.ini_msgChatHandle.click_chatPageBackBtn()
            self.logger.log('调用:聊天界面-返回, 返回聊天记录界面', 'info')
            assert self.handle.ini_msgChatHandle.judgment_page_single(
                self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())

            self.handle.init_msgChatSetHandle.click_groupChatSetClearHistoryMsg()
            self.logger.log('群聊设置界面-清空聊天记录', 'info')
            assert self.handle.ini_msgChatHandle.judgment_checked(
                self.handle.init_msgChatSetHandle.get_groupChatSetHistoryMsgTitle())
            self.handle.init_msgChatSetHandle.click_groupChatSetClearHistoryMsgOk()
            self.logger.log('群聊设置界面-清空聊天记录-确定按钮', 'info')
            if self.handle.init_msgChatSetHandle.judgment_toast("操作"):
                print("getEle")

            self.handle.init_msgChatSetHandle.click_groupChatSetDismissGroup()
            self.logger.log('群聊设置界面-解散群聊', 'info')

            self.handle.init_msgChatSetHandle.click_groupChatSetClearHistoryMsgOk()
            self.logger.log('解散群聊-确定按钮', 'info')

            if self.handle.init_infor_handle.judgment_page_single(
                    self.handle.init_infor_handle.get_messageModuleQueryBoxCle()):
                self.handle.init_infor_handle.click_messageModuleQueryBoxCle()
                self.logger.log('消息模块界面-点击文本框清除按钮', 'info')
                time.sleep(1)
                assert self.handle.ini_msgChatHandle.judgment_page_single(
                    self.handle.init_infor_handle.get_averGroupEleTemp()) == False
                self.logger.log('断言消息模块界面,群聊名称', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            if self.handle.init_infor_handle.judgment_page_single(
                    self.handle.init_infor_handle.get_messageModuleQueryBoxCle()):
                self.handle.init_infor_handle.click_messageModuleQueryBoxCle()
                self.logger.log('消息模块界面-点击文本框清除按钮', 'info')
            else:
                try:
                    self.handle.ini_msgChatHandle.writeIn_msgPage_query(caseGroupNickName['group_nick_case'])
                    self.logger.log('查询案件群聊', 'info')
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.ini_msgChatHandle.get_group_info())
                    self.logger.log('查询案件群聊,断言查询结果', 'info')
                except Exception as e:
                    raise self.logger.log(f'\n{e}!', 'error')

                try:
                    self.handle.ini_msgChatHandle.click_group_info()
                    self.logger.log('查询已创建群聊,进入聊天界面', 'info')
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.ini_msgChatHandle.get_chat_page_cant())
                    self.logger.log('查询已创建群聊,进入聊天界面,断言"暗语"唯一元素', 'info')

                    self.handle.init_msgChatSetHandle.click_msgChatSetBtn()
                    self.logger.log('进入群聊设置界面', 'info')
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.init_msgChatSetHandle.get_group_chat_set()) == False
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.init_msgChatSetHandle.get_groupChatSetUpTopSwitch())

                    self.handle.init_msgChatSetHandle.click_groupChatSetDismissGroup()
                    self.logger.log('群聊设置界面-解散群聊', 'info')

                    self.handle.init_msgChatSetHandle.click_groupChatSetClearHistoryMsgOk()
                    self.logger.log('解散群聊-确定按钮', 'info')

                    self.handle.init_infor_handle.click_messageModuleQueryBoxCle()
                    self.logger.log('消息模块界面-点击文本框清除按钮', 'info')

                    time.sleep(1)
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.ini_msgChatHandle.get_msgPage_query())
                    assert self.handle.ini_msgChatHandle.judgment_page_single(
                        self.handle.init_infor_handle.get_averGroupEleCase()) == False
                    self.logger.log('断言消息模块界面,群聊名称', 'info')
                    self.logger.log('断言"消息"界面搜索框', 'info')

                except Exception as e:
                    raise self.logger.log(f'\n{e}!', 'error')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('06')
    # @pytest.mark.skip(reason="跳过进入通讯录界面")
    @allure.story('通讯录')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入通讯录界面..')
    @allure.severity("critical")
    @pytest.mark.parametrize('contactList', Base.gain_write_RenameGroupOwner('contactList', 'contactListBox'))
    def test_contactList(self, contactList):
        try:
            self.handle.init_contactList.click_bottomNavContactListIcon()
            self.logger.log('断言消息模块界面,群聊名称', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_contactListTopTitleText())
            self.logger.log('断言通讯录模块,置顶通栏文案', 'info')

            self.handle.init_contactList.write_contactListSearchBox(contactList['content'])
            self.logger.log('文本框搜索固定文案', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_contactListMemberIcon())
            self.logger.log('断言通讯录模块,置顶通栏文案', 'info')

            self.handle.init_contactList.click_contactListMemberIcon()
            self.logger.log('点击查询结果人员的头像', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_contactListMemberName())
            self.logger.log('断言查询结果人员的名称', 'info')

            self.handle.init_contactList.click_contactListSendMsg()
            self.logger.log('人员详情页,点击"发消息"', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatTopText())
            self.logger.log('断言人员聊天界面Title文案', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('07')
    # @pytest.mark.skip(reason="跳过成员聊天室")
    @allure.story('成员聊天室')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入成员聊天界面..')
    @allure.severity("trivial")
    @pytest.mark.parametrize('contactList', Base.gain_write_RenameGroupOwner('contactList', 'contactListBox'))
    def test_contactList_memberChat(self, contactList):
        try:
            self.handle.init_contactList.write_memberChitchatBottomMsgTextBox(contactList['content'])
            self.logger.log('成员聊天界面->输入文本信息', 'info')
            self.handle.init_contactList.click_memberChitchatDetails_btn_send()
            self.logger.log('成员聊天界面->点击发送消息按钮', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_audio_text())
            self.logger.log('成员聊天界面->文字发送成功', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatMorePhoto())
            self.logger.log('断言成员聊天界面->更多->相册', 'info')

            '''if self.handle.init_contactList.judgment_page_single(self.handle.init_contactList.get_memberChitchatDetails_ll_clear()):
                self.handle.init_contactList.click_memberChitchat_ll_clear()
                self.logger.log('成员聊天界面->一键清除', 'info')
                assert self.handle.init_contactList.judgment_page_single(
                    self.handle.init_contactList.get_memberChitchatDetails_tv_ok())
                self.logger.log('断言,成员聊天界面->一键清除,确认弹窗', 'info')
                self.handle.init_contactList.click_memberChitchat_tv_ok()
                self.logger.log('成员聊天界面->一键清除,点击确认弹窗', 'info')
                assert self.handle.init_contactList.judgment_page_single(
                    self.handle.init_contactList.get_memberChitchatDetails_audio_text()) == False
                self.logger.log('断言,成员聊天界面->一键清除,确认弹窗', 'info')'''

            self.handle.init_contactList.click_memberChitchatBottomVoiceBtn()
            self.logger.log('成员聊天界面->点击语音按钮', 'info')
            time.sleep(1)
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatBottomPressToSpeak())
            self.logger.log('断言,成员聊天界面->点击语音按钮', 'info')
            self.handle.init_contactList.click_memberChitchatBottomPressToSpeak()
            self.logger.log('成员聊天界面->常按住说话', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_audio_send())
            self.logger.log('断言,成员聊天界面->已发送语音', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_chat_error()) == False
            self.logger.log('断言,成员聊天界面->消息发送失败', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')
            self.handle.init_contactList.click_memberChitchatMorePhoto()
            self.logger.log('成员聊天界面->更多->点击相册', 'info')

            if self.handle.init_contactList.judgment_page_single(
                    self.handle.init_contactList.get_memberChitchatMorePhotoCheck()):
                self.logger.log('判断成员聊天界面->更多->相册是否为空', 'info')
                self.handle.init_contactList.click_memberChitchatMorePhotoCheck()
                self.logger.log('成员聊天界面->更多->相册->点击复选框', 'info')
                assert self.handle.init_contactList.judgment_checked(
                    self.handle.init_contactList.get_memberChitchatMorePhotoCheck())
                self.handle.init_contactList.click_memberChitchatMorePhotoCheckCompleteSelect()
                self.logger.log('成员聊天界面->更多->相册->复选框->点击已完成', 'info')
            else:
                self.handle.init_contactList.click_memberChitchatLeftBack()
                self.logger.log('成员聊天界面->更多->相册->点击返回按钮', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')

            self.handle.init_contactList.click_memberChitchatLocation()
            self.logger.log('成员聊天界面->更多->点击发送位置', 'info')
            time.sleep(1)
            assert self.handle.init_contactList.judgment_checked(
                self.handle.init_contactList.get_memberChitchatLocationSendBtn())
            self.logger.log('断言,位置界面->发送按钮', 'info')
            time.sleep(1)
            self.handle.init_contactList.click_memberChitchatLocationSendBtn()
            self.logger.log('位置界面->点击发送按钮', 'info')
            assert self.handle.init_contactList.judgment_checked(
                self.handle.init_contactList.get_memberChitchatDetails_location_send())
            self.logger.log('断言,成员聊天界面->位置发送成功', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_chat_error()) == False
            self.logger.log('断言,成员聊天界面->消息发送失败', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')

            self.handle.init_contactList.click_memberChitchatAudioCall()
            self.logger.log('成员聊天界面->更多->点击音频通话', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatCancel())
            self.logger.log('断言,成员聊天界面->更多->音频通话->挂断', 'info')
            time.sleep(3)
            self.handle.init_contactList.click_memberChitchatCancel()
            self.logger.log('成员聊天界面->更多->音频通话->点击挂断', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_audio_text())
            self.logger.log('断言,成员聊天界面->更多->音频通话->挂断消息', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')

            self.handle.init_contactList.click_memberChitchatVideoCall()
            self.logger.log('成员聊天界面->更多->点击视频通话', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatVideoCallCancel())
            self.handle.init_contactList.click_memberChitchatVideoCallCancel()
            self.logger.log('成员聊天界面->更多->视频通话->点击挂断', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_audio_text())
            self.logger.log('断言,成员聊天界面->更多->视频通话->挂断消息', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')

            self.handle.init_contactList.click_memberChitchatUserRead()
            self.logger.log('成员聊天界面->更多->点击阅后即焚', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatBurnAfterReading())
            self.logger.log('断言成员聊天界面->阅后即焚', 'info')

            self.handle.init_contactList.click_memberChitchatBottomVoiceBtn()
            self.logger.log('成员聊天界面->点击语音按钮', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatBottomPressToSpeak())
            self.logger.log('断言,成员聊天界面->按住说话', 'info')

            self.handle.init_contactList.click_memberChitchatBottomPressToSpeak()
            self.logger.log('成员聊天界面->长按说话', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDetails_audio_send())
            self.logger.log('断言,成员聊天界面->长按说话', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

        try:
            self.handle.init_contactList.click_memberChitchatBurnAfterReadingClose()
            self.logger.log('成员聊天界面->阅后即焚->点击关闭', 'info')

            self.handle.init_contactList.click_memberChitchatBottomMoreBtn()
            self.logger.log('成员聊天界面->更多按钮', 'info')

            self.handle.init_contactList.click_memberChitchatUserShare()
            self.logger.log('成员聊天界面->更多->点击设备分享', 'info')
            assert self.handle.init_contactList.judgment_page_single(
                self.handle.init_contactList.get_memberChitchatDeviceShare())
            self.logger.log('断言,设备分享页,Title文案', 'info')

            if self.handle.init_contactList.judgment_page_single(
                    self.handle.init_contactList.get_memberChitchatDeviceShareList()):
                self.handle.init_contactList.click_memberChitchatDeviceShareListCheck()
                self.logger.log('成员聊天界面->更多->点击设备分享复选框', 'info')
                assert self.handle.init_contactList.judgment_checked(
                    self.handle.init_contactList.get_memberChitchatDeviceShareBtn())
                self.logger.log('断言,成员聊天界面->更多->点击设备分享复选框,勾选状态', 'info')

                self.handle.init_contactList.click_memberChitchatDeviceShareBtn()
                self.logger.log('点击成员聊天界面->更多->点击设备分享按钮', 'info')

                assert self.handle.init_contactList.judgment_checked(
                    self.handle.init_contactList.get_memberChitchatDeviceShareSucceed())
                self.logger.log('设备分享成功', 'info')

                for _ in range(2):
                    self.handle.init_contactList.click_memberChitchatTopBack()
                self.logger.log('成员聊天界面->点击返回按钮', 'info')
            else:
                self.handle.init_contactList.click_memberChitchatTopBack()
                self.logger.log('返回', 'info')
        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('08')
    # @pytest.mark.skip(reason="跳过实时侦控")
    @allure.story('应用 - 实时侦控')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入: 应用 - 实时侦控..')
    @allure.severity("critical")
    # @pytest.mark.parametrize('contactList', Base.gain_write_RenameGroupOwner('contactList', 'contactListBox'))
    def test_monitoringRealTime(self):
        try:
            self.handle.init_monitoringRealTime.click_applyButtomIcon()
            self.logger.log('点击底部导航栏: "应用"', 'info')
            assert self.handle.init_monitoringRealTime.judgment_checked(
                self.handle.init_monitoringRealTime.get_monitoringRealTime())

            self.handle.init_monitoringRealTime.click_monitoringRealTime()
            self.logger.log('点击 "应用-实时侦控Icon"', 'info')
            assert self.handle.init_monitoringRealTime.judgment_checked(
                self.handle.init_monitoringRealTime.get_monitoringRealTime())

            self.handle.init_monitoringRealTime.click_monitoringRealTimeDevice()
            self.logger.log('点击实时侦控->设备封面"', 'info')
            if self.handle.init_monitoringRealTime.judgment_page_single(
                    self.handle.init_monitoringRealTime.get_monitoringRealTimeDeviceDtailscChannel()):
                self.logger.log('进入直播间,判断是否有"通道"', 'info')
                # self.handle.init_monitoringRealTime.click_monitoringRealTimeDeviceDtailscPhoto()
                self.handle.init_monitoringRealTime.click_monitoringRealTimeDeviceDtailscBack()
                self.logger.log('实时侦控->设备详情页面返回', 'info')

            if self.handle.init_monitoringRealTime.judgment_page_single(self.handle.init_monitoringRealTime.get_monitoringRealTimeToConsult()):
                self.logger.log('判断是否调阅', 'info')
                self.handle.init_monitoringRealTime.click_monitoringRealTimeToConsultCancel()
                self.logger.log('实时侦控->取消调阅', 'info')
                self.handle.init_monitoringRealTime.click_monitoringRealTimeDeviceListBack()
                self.logger.log('实时侦控->设备列表页面返回', 'info')
            else:
                self.handle.init_monitoringRealTime.click_monitoringRealTimeDeviceDtailscBack()
                assert self.handle.init_monitoringRealTime.judgment_checked(
                    self.handle.init_monitoringRealTime.get_monitoringRealTime())
                self.handle.init_monitoringRealTime.click_monitoringRealTimeDeviceListBack()
                assert self.handle.init_monitoringRealTime.judgment_checked(
                    self.handle.init_monitoringRealTime.get_monitoringRealTime())
                self.logger.log('点击返回', 'info')

        except Exception as e:
            raise self.logger.log(f'\n{e}!', 'error')

    @allure.tag('09')
    # @pytest.mark.skip(reason="跳过紧急报警")
    @allure.epic('测试对象: 应用-紧急报警')
    @allure.story('应用-紧急报警')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入: 应用-紧急报警..')
    @allure.severity("critical")
    def test_emergencyAlarm(self):
        for _ in range(2):
            self.handle.init_emergencyAlarmHandle.click_emergencyAlarmIcon()
            self.logger.log('点击紧急报警Icon"', 'info')

    @allure.tag('10')
    # @pytest.mark.skip(reason="应用-直播间")
    @allure.epic('测试对象: 应用-直播间')
    @allure.story('应用')
    @allure.issue(r'Bug管理平台', 'https://www.ZenTao.com')
    @allure.description('进入: 应用-直播间..')
    @allure.severity("critical")
    # @pytest.mark.parametrize('contactList', Base.gain_write_RenameGroupOwner('contactList', 'contactListBox'))
    def test_startLiveStreaming(self):
        self.handle.init_startLiveStreamingHandle.click_startLiveStreamingIcon()
        self.logger.log('点击进入直播间', 'info')
        assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingPhotoAlbum())
        self.logger.log('断言,直播间->已经保存视图文件入口', 'info')

        self.handle.init_startLiveStreamingHandle.click_startLiveStreamingDevSetting()
        self.logger.log('直播间->点击设置', 'info')
        assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingLowBatteryWarning())
        self.logger.log('断言,直播间->设置->功能设置->低电量预警', 'info')

        self.handle.init_startLiveStreamingHandle.click_startLiveStreamingMotionDetection()
        self.logger.log('直播间->设置->功能设置->移动侦测', 'info')
        assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch())
        self.logger.log('断言,直播间->设置->功能设置->开关', 'info')

        if not self.handle.init_startLiveStreamingHandle.judgment_checked(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch()):
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingMotionDetectionSwitch()
            assert self.handle.init_startLiveStreamingHandle.judgment_checked(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch())
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->开关', 'info')
            assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingVoiceMonitor())
            self.logger.log('断言,直播间->设置->功能设置->声压监测', 'info')
        else:
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->功能设置->返回', 'info')

        self.handle.init_startLiveStreamingHandle.click_startLiveStreamingVoiceMonitor()
        self.logger.log('直播间->设置->功能设置->声压监测', 'info')
        assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch())
        self.logger.log('断言,直播间->设置->功能设置->开关', 'info')
        if not self.handle.init_startLiveStreamingHandle.judgment_checked(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch()):
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingMotionDetectionSwitch()
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->功能设置->开关', 'info')
        else:
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->返回', 'info')
            assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingObjectRecognition())
            self.logger.log('断言,直播间->设置->功能设置->物体识别', 'info')

        self.handle.init_startLiveStreamingHandle.click_startLiveStreamingObjectRecognition()
        self.logger.log('直播间->设置->功能设置->点击物体识别', 'info')
        assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch())
        self.logger.log('断言,直播间->设置->功能设置->开关', 'info')

        if not self.handle.init_startLiveStreamingHandle.judgment_checked(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingMotionDetectionSwitch()):
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingMotionDetectionSwitch()
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->功能设置->开关', 'info')
        else:
            self.handle.init_startLiveStreamingHandle.click_startLiveStreamingSettingBack()
            self.logger.log('直播间->设置->返回', 'info')
            assert self.handle.init_startLiveStreamingHandle.judgment_page_single(self.handle.init_startLiveStreamingHandle.get_startLiveStreamingLowBatteryWarning())
            self.logger.log('断言,直播间->设置->功能设置->低电量预警', 'info')

    # 资源释放
    def teardown_class(self):
        """
        3: HOME键，返回到主屏幕
        4: BACK键，返回上一个界面
        5: 拨号键
        6: 挂断键
        19: 上键
        20: 下键
        21: 左键
        22: 右键
        23: 确认/确定键
        24: 音量增加键
        25: 音量减少键
        26: 电源键
        27: 拍照键
        66: 回车键
        82: 菜单键
        84: 搜索键
        164: 静音键
        83: 打开通知栏
        """
        time.sleep(2)
        if self.driver:
            self.logger.log('\n' + '\n' + '资源释放', 'info')
            self.driver.quit()
            self.driver = None
            os.system('adb shell input keyevent 26')
            self.logger.log('\n' + '启用锁屏', 'info')
        else:
            self.driver = None
