#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import logging
import sys


def log_decorator(func):
    def wrapper(*args, **kwargs):
        logger = Logger()
        logger.log(f"Calling function {func.__name__}")
        try:
            result = func(*args, **kwargs)
            logger.log(f"Function {func.__name__} executed successfully")
            return result
        except Exception as e:
            logger.log(f"Error executing function {func.__name__}: {str(e)}", 'error')
            raise

    return wrapper


class Logger():
    def __init__(self, logger_name=__name__, file_name='./log/log.log', log_level=logging.DEBUG):
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(log_level)
        formatter = logging.Formatter("\n"'%(asctime)s.%(msecs)03d - %(name)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        fh = logging.FileHandler(file_name, encoding='utf-8')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        self.logger.addHandler(sh)

        # 添加全局的异常处理器
        sys.excepthook = self.handle_uncaught_exception

    def handle_uncaught_exception(self, exc_type, exc_value, exc_traceback):
        self.logger.error('Uncaught exception', exc_info=(exc_type, exc_value, exc_traceback))

    def log(self, msg, level='info'):
        level_map = {
            'debug': logging.DEBUG,
            'info': logging.INFO,
            'warning': logging.WARNING,
            'error': logging.ERROR,
            'critical': logging.CRITICAL
        }
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)
        caller_name = calframe[1][3]
        caller_line = calframe[1][2]
        level = level.lower()
        if level in level_map:
            self.logger.log(level_map[level], f'{caller_name} - line {caller_line} - {msg}')
        else:
            self.logger.error(f'line {caller_line} - Invalid log level: {level}')
