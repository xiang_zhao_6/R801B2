"""
    :account for: 元素层
    :modules: 通讯录页面
"""
import Base
from selenium.webdriver.common.by import By


class ContactListPage(Base.MethodDepot):
    bottomNavContactListIcon = By.ID, "com.zbgd.r801b2:id/ivContact"  # 底部通讯录icon
    contactListSearchBox = By.ID, "com.zbgd.r801b2:id/etContent"  # 置顶搜索框
    contactListFriendsTab = By.ID, "com.zbgd.r801b2:id/tv_friends"  # Tab左->好友
    contactListGroupTab = By.ID, "com.zbgd.r801b2:id/tv_group"  # Tab右->群聊
    contactListMemberHeadPortrait = By.ID, "com.zbgd.r801b2:id/ivHead"  # 用户列表头像Icon
    contactListSendMsg = By.ID, "com.zbgd.r801b2:id/layoutChat"  # 人员详情页->发消息
    contactListTopTitleText = By.XPATH, "//*[contains(@text,'通讯录')]"  # 置顶通栏文案
    contactListMemberName = By.XPATH, "//*[contains(@text,'陶')]"  # 人员详情页->姓名
    contactListMemberIcon = By.ID, "com.zbgd.r801b2:id/ivHead"  # 人员头像

    def get_bottomNavContactListIcon(self):
        """
        底部通讯录icon
        :return: Element
        """
        return self.get_element(self.bottomNavContactListIcon)

    def get_contactListSearchBox(self):
        """
        置顶搜索框
        :return: Element
        """
        return self.get_element(self.contactListSearchBox)

    def get_contactListFriendsTab(self):
        """
        Tab左->好友
        :return: Element
        """
        return self.get_element(self.contactListFriendsTab)

    def get_contactListGroupTab(self):
        """
        Tab右->群聊
        :return: Element
        """
        return self.get_element(self.contactListGroupTab)

    def get_contactListMemberHeadPortrait(self):
        """
        用户列表头像Icon
        :return: Element
        """
        return self.get_element(self.contactListMemberHeadPortrait)

    def get_contactListSendMsg(self):
        """
        人员详情页->发消息
        :return: Element
        """
        return self.get_element(self.contactListSendMsg)

    def get_contactListTopTitleText(self):
        """
        置顶通栏文案
        :return: Element
        """
        return self.get_element(self.contactListTopTitleText)

    def get_contactListMemberName(self):
        """
        人员详情页->姓名
        :return: Element
        """
        return self.get_element(self.contactListMemberName)

    def get_contactListMemberIcon(self):
        """
        人员头像
        :return: Element
        """
        return self.get_element(self.contactListMemberIcon)
