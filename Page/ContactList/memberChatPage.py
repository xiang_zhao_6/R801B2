"""
    :account for: 元素层
    :modules: 通讯录->成员聊天界面
"""
import Base
from selenium.webdriver.common.by import By


class MemberChitchatPage(Base.MethodDepot):
    memberChitchatTopRightBtn = By.ID, "com.zbgd.r801b2:id/ivRight"  # 成员聊天界面->右上角按钮
    memberChitchatBottomVoiceBtn = By.ID, "com.zbgd.r801b2:id/btn_set_mode_voice"  # 成员聊天界面->语音按钮
    memberChitchatBottomMsgTextBox = By.ID, "com.zbgd.r801b2:id/et_message"  # 成员聊天界面->消息框
    memberChitchatBottomMoreBtn = By.ID, "com.zbgd.r801b2:id/btn_more"  # 成员聊天界面->更多按钮
    memberChitchatBottomArgot = By.ID, "com.zbgd.r801b2:id/ll_language"  # 成员聊天界面->暗语
    memberChitchatBottomPressToSpeak = By.ID, "com.zbgd.r801b2:id/btn_press_to_speak"  # 成员聊天界面->按住说话
    memberChitchatTopBack = By.ID, "com.zbgd.r801b2:id/ivBack"  # 返回按钮
    memberChitchatTopText = By.XPATH, "//*[contains(@text,'陶')]"  # 成员聊天界面->成员名称
    memberChitchatMorePhoto = By.ID, "com.zbgd.r801b2:id/btn_photo"  # 成员聊天界面->更多->相册
    memberChitchatMorePhotoCheck = By.ID, "com.zbgd.r801b2:id/tvCheck"  # 成员聊天界面->更多->相册->复选框
    memberChitchatMorePhotoCheckCompleteSelect = By.ID, "com.zbgd.r801b2:id/ps_complete_select"  # 成员聊天界面->更多->相册->复选框->已完成
    memberChitchatMorePhotoCheckLeftBack = By.ID, "com.zbgd.r801b2:id/ps_iv_left_back"  # 成员聊天界面->更多->相册->返回
    memberChitchatCamera = By.ID, "com.zbgd.r801b2:id/btn_camera"  # 成员聊天界面->更多->拍摄
    memberChitchatFile = By.ID, "com.zbgd.r801b2:id/btn_file"  # 成员聊天界面->更多->文件
    memberChitchatLocation = By.ID, "com.zbgd.r801b2:id/btn_location"  # 成员聊天界面->更多->发送位置
    memberChitchatAudioCall = By.ID, "com.zbgd.r801b2:id/btn_audio_call"  # 成员聊天界面->更多->音频通话
    memberChitchatcCancel = By.ID, "com.zbgd.r801b2:id/ic_cancel"  # 成员聊天界面->更多->音频通话->挂断
    memberChitchatVideoCall = By.ID, "com.zbgd.r801b2:id/btn_video_call"  # 成员聊天界面->更多->视频通话
    memberChitchatVideoCallCancel = By.ID, "com.zbgd.r801b2:id/le_refuse"  # 成员聊天界面->更多->视频通话->挂断电话
    memberChitchatUserRead = By.ID, "com.zbgd.r801b2:id/btn_user_read"  # 成员聊天界面->更多->阅后即焚
    memberChitchatUserShare = By.ID, "com.zbgd.r801b2:id/btn_user_share"  # 成员聊天界面->更多->设备分享
    memberChitchatDetailsbtn_send = By.ID, "com.zbgd.r801b2:id/btn_send"  # 成员聊天界面->发送消息按钮
    memberChitchatDetails_audio_send = By.ID, "com.zbgd.r801b2:id/message_audio_send_ll"  # 成员聊天界面->语音发送成功
    memberChitchatDetails_chat_error = By.ID, "com.zbgd.r801b2:id/iv_chat_error"  # 成员聊天界面->消息发送失败
    memberChitchatDetails_audio_text = By.ID, "com.zbgd.r801b2:id/message_text"  # 成员聊天界面->文字发送成功
    memberChitchatDetails_location_send = By.ID, "com.zbgd.r801b2:id/message_location_send"  # 成员聊天界面->位置发送成功
    memberChitchatDetails_ll_clear = By.ID, "com.zbgd.r801b2:id/ll_clear"  # 成员聊天界面->一键清除
    memberChitchatDetails_tv_ok = By.ID, "com.zbgd.r801b2:id/tv_ok"  # 成员聊天界面->一键清除->确认按钮
    memberChitchatLocationSendBtn = By.XPATH, "//*[contains(@text,'发送')]"  # 位置界面->发送按钮
    memberChitchatBurnAfterReading = By.XPATH, "//*[contains(@text,'阅后')]"  # 阅后即焚
    memberChitchatDeviceShare = By.XPATH, "//*[contains(@text,'设备分享')]"  # 设备分享页,Title文案
    memberChitchatBurnAfterReadingClose = By.XPATH, "//*[contains(@text,'关闭')]"  # 阅后即焚->关闭
    memberChitchatDeviceShareList = By.ID, "com.zbgd.r801b2:id/root_layout"  # 设备分享页,设备元素列表
    memberChitchatDeviceShareListCheck = By.ID, "com.zbgd.r801b2:id/iv_check"  # 设备分享页,设备元素列表->复选框
    memberChitchatDeviceShareBtn = By.ID, "com.zbgd.r801b2:id/btn_next"  # 设备分享页,设备元素列表->分享
    memberChitchatDeviceShareSucceed = By.ID, "com.zbgd.r801b2:id/message_img_send"  # 设备分享成功

    def get_memberChitchatTopRightBtn(self):
        """
        成员聊天界面->右上角按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatTopRightBtn)

    def get_memberChitchatBottomVoiceBtn(self):
        """
        成员聊天界面->语音按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatBottomVoiceBtn)

    def get_memberChitchatBottomMsgTextBox(self):
        """
        成员聊天界面->消息框
        :return: Element
        """
        return self.get_element(self.memberChitchatBottomMsgTextBox)

    def get_memberChitchatBottomMoreBtn(self):
        """
        成员聊天界面->更多按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatBottomMoreBtn)

    def get_memberChitchatBottomArgot(self):
        """
        成员聊天界面->暗语
        :return: Element
        """
        return self.get_element(self.memberChitchatBottomArgot)

    def get_memberChitchatBottomPressToSpeak(self):
        """
        成员聊天界面->按住说话
        :return: Element
        """
        return self.get_element(self.memberChitchatBottomPressToSpeak)

    def get_memberChitchatTopBack(self):
        """
        返回按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatTopBack)

    def get_memberChitchatTopText(self):
        """
        成员聊天界面->成员名称
        :return: Element
        """
        return self.get_element(self.memberChitchatTopText)

    def get_memberChitchatMorePhoto(self):
        """
        成员聊天界面->更多->相册
        :return: Element
        """
        return self.get_element(self.memberChitchatMorePhoto)

    def get_memberChitchatMorePhotoCheck(self):
        """
        成员聊天界面->更多->相册->复选框
        :return: Element
        """
        return self.get_element(self.memberChitchatMorePhotoCheck)

    def get_memberChitchatMorePhotoCheckCompleteSelect(self):
        """
        成员聊天界面->更多->相册->复选框->已完成
        :return: Element
        """
        return self.get_element(self.memberChitchatMorePhotoCheckCompleteSelect)

    def get_memberChitchatMorePhotoCheckLeftBack(self):
        """
        成员聊天界面->更多->相册->返回
        :return: Element
        """
        return self.get_element(self.memberChitchatMorePhotoCheckLeftBack)

    def get_memberChitchatCamera(self):
        """
        成员聊天界面->更多->拍摄
        :return: Element
        """
        return self.get_element(self.memberChitchatCamera)

    def get_memberChitchatFile(self):
        """
        成员聊天界面->更多->文件
        :return: Element
        """
        return self.get_element(self.memberChitchatFile)

    def get_memberChitchatLocation(self):
        """
        成员聊天界面->更多->发送位置
        :return: Element
        """
        return self.recursion_element_down(self.memberChitchatLocation)

    def get_memberChitchatLocationSendBtn(self):
        """
        位置界面->发送按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatLocationSendBtn)

    def get_memberChitchatAudioCall(self):
        """
        成员聊天界面->更多->音频通话
        :return: Element
        """
        return self.get_element(self.memberChitchatAudioCall)

    def get_memberChitchatCancel(self):
        """
        成员聊天界面->更多->音频通话->挂断
        :return: Element
        """
        return self.get_element(self.memberChitchatcCancel)

    def get_memberChitchatVideoCall(self):
        """
        成员聊天界面->更多->视频通话
        :return: Element
        """
        return self.get_element(self.memberChitchatVideoCall)

    def get_memberChitchatVideoCallCancel(self):
        """
        成员聊天界面->更多->视频通话->挂断电话
        :return: Element
        """
        return self.get_element(self.memberChitchatVideoCallCancel)

    def get_memberChitchatUserRead(self):
        """
        成员聊天界面->更多->阅后即焚
        :return: Element
        """
        return self.get_element(self.memberChitchatUserRead)

    def get_memberChitchatUserShare(self):
        """
        成员聊天界面->更多->设备分享
        :return: Element
        """
        return self.get_element(self.memberChitchatUserShare)

    def get_memberChitchatDetailsSendMsg(self):
        """
        成员聊天界面->人员详情->发消息
        :return: Element
        """
        return self.get_element(self.memberChitchatDetailsSendMsg)

    def get_memberChitchatDetails_btn_send(self):
        """
        成员聊天界面->发送消息按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatDetailsbtn_send)

    def get_memberChitchatDetails_audio_send(self):
        """
        成员聊天界面->语音发送成功
        :return: Element
        """
        return self.get_element(self.memberChitchatDetails_audio_send)

    def get_memberChitchatDetails_chat_error(self):
        """
        成员聊天界面->消息发送失败
        :return: Element
        """
        return self.get_element(self.memberChitchatDetails_chat_error)

    def get_memberChitchatDetails_audio_text(self):
        """
        成员聊天界面->文字发送成功
        :return: Element
        """
        return self.get_element(self.memberChitchatDetails_audio_text)

    def get_memberChitchatDetails_location_send(self):
        """
        成员聊天界面->位置发送成功
        :return: Element
        """
        return self.get_element(self.memberChitchatDetails_location_send)

    def get_memberChitchatDetails_ll_clear(self):
        """
        成员聊天界面->一键清除
        :return: Element
        """
        return self.recursion_element(self.memberChitchatDetails_ll_clear)

    def get_memberChitchatDetails_tv_ok(self):
        """
        成员聊天界面->一键清除->确认按钮
        :return: Element
        """
        return self.get_element(self.memberChitchatDetails_tv_ok)

    def get_memberChitchatBurnAfterReading(self):
        """
        成员聊天界面->阅后即焚
        :return: Element
        """
        return self.get_element(self.memberChitchatBurnAfterReading)

    def get_memberChitchatBurnAfterReadingClose(self):
        """
        成员聊天界面->阅后即焚->关闭
        :return: Element
        """
        return self.get_element(self.memberChitchatBurnAfterReadingClose)

    def get_memberChitchatDeviceShare(self):
        """
        设备分享页,Title文案
        :return: Element
        """
        return self.get_element(self.memberChitchatDeviceShare)

    def get_memberChitchatDeviceShareList(self):
        """
        设备分享页,设备元素列表
        :return: Element
        """
        return self.get_element(self.memberChitchatDeviceShareList)

    def get_memberChitchatDeviceShareListCheck(self):
        """
        设备分享页,设备元素列表->复选框
        :return: Element
        """
        return self.get_element(self.memberChitchatDeviceShareListCheck)

    def get_memberChitchatDeviceShareBtn(self):
        """
        设备分享页,设备元素列表->分享
        :return: Element
        """
        return self.get_element(self.memberChitchatDeviceShareBtn)

    def get_memberChitchatDeviceShareSucceed(self):
        """
        设备分享成功
        :return: Element
        """
        return self.get_element(self.memberChitchatDeviceShareSucceed)
