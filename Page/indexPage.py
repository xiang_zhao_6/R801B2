"""
    :account for: 动作层
    :modules: 应用锁
"""
from selenium.webdriver.common.by import By
import Base


class IndexPage(Base.MethodDepot):
    lock_screen_sw_server = By.ID, "com.zbgd.r801b2:id/le_address"
    input_server = By.ID, "com.zbgd.r801b2:id/et_server"
    input_port = By.ID, "com.zbgd.r801b2:id/et_port"
    input_but = By.ID, "com.zbgd.r801b2:id/bt_confirm"
    keypad_five = By.ID, "com.zbgd.r801b2:id/user_password"

    def get_lock_screen_server(self):
        """
        锁屏页_切换服务器
        :return: Element
        """
        return self.get_element(self.lock_screen_sw_server)

    def get_input_server(self):
        """
        切换服务器地址页_输入IP
        :return: Element
        """
        return self.get_element(self.input_server)

    def get_input_port(self):
        """
        切换服务器地址页_输入port
        :return: Element
        """
        return self.get_element(self.input_port)

    def get_input_but(self):
        """
        切换服务器地址页_保存按钮
        :return: Element
        """
        return self.get_element(self.input_but)

    def get_keypad_five(self):
        """
        应用锁键盘
        :return: Element
        """
        return self.get_element(self.keypad_five)
