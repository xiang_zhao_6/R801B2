"""
    :account for: 元素层
    :modules: 应用-紧急报警
"""
import Base
from selenium.webdriver.common.by import By


class EmergencyAlarmPage(Base.MethodDepot):
    emergencyAlarmIcon = By.XPATH, "//*[contains(@text, '紧急报警')]"  # 紧急报警
    emergencyAlarmSwitch = By.ID, "com.zbgd.r801b2:id/iv_open"  # 紧急报警-开关

    def get_emergencyAlarmIcon(self):
        """
        紧急报警Icon
        :return: Element
        """
        return self.get_element(self.emergencyAlarmIcon)

    def get_emergencyAlarmSwitch(self):
        """
        紧急报警-开关
        :return: Element
        """
        return self.get_element(self.emergencyAlarmSwitch)
