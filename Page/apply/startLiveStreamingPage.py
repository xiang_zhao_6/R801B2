"""
    :account for: 动作层
    :modules: 应用-开启直播
"""
from selenium.webdriver.common.by import By

import Base


class StartLiveStreamingPage(Base.MethodDepot):
    startLiveStreamingIcon = By.XPATH, "//*[contains(@text, '开启直播')]"  # 开启直播
    startLiveStreamingPhotoAlbum = By.ID, "com.zbgd.r801b2:id/photo_album"  # 直播间->已经保存视图文件入口
    startLiveStreamingRecord = By.ID, "com.zbgd.r801b2:id/iv_record"  # 直播间->录制视频
    startLiveStreamingPhoto = By.ID, "com.zbgd.r801b2:id/iv_photo"  # 直播间->截图
    startLiveStreamingFullScreen = By.ID, "com.zbgd.r801b2:id/iv_fullScreen"  # 直播间->全屏
    startLiveStreamingDevShare = By.ID, "com.zbgd.r801b2:id/device_share"  # 直播间->设备分享
    startLiveStreamingDevSetting = By.ID, "com.zbgd.r801b2:id/ic_setting"  # 直播间->设置
    startLiveStreamingBack = By.ID, "com.zbgd.r801b2:id/ic_back"  # 直播间->返回
    startLiveStreamingSettingBack = By.ID, "com.zbgd.r801b2:id/ivBack"  # 直播间->设置->返回
    startLiveStreamingMotionDetection = By.ID, "com.zbgd.r801b2:id/con_move"  # 直播间->设置->功能设置->移动侦测
    startLiveStreamingMotionDetectionSwitch = By.ID, "com.zbgd.r801b2:id/check_detection"  # 直播间->设置->功能设置->开关
    startLiveStreamingMotionDetectionConfirm = By.ID, "com.zbgd.r801b2:id/tvRight"  # 直播间->设置->功能设置->确定按钮
    startLiveStreamingVoiceMonitor = By.ID, "com.zbgd.r801b2:id/con_sound"  # 直播间->设置->功能设置->声压监测
    startLiveStreamingObjectRecognition = By.ID, "com.zbgd.r801b2:id/con_thing"  # 直播间->设置->功能设置->物体识别
    startLiveStreamingCheckAll = By.ID, "com.zbgd.r801b2:id/check_all"  # 直播间->设置->功能设置->物体识别->全选
    startLiveStreamingLowBatteryWarning = By.ID, "com.zbgd.r801b2:id/con_low_battery"  # 直播间->设置->功能设置->低电量预警
    startLiveStreamingOptions = By.ID, "com.zbgd.r801b2:id/tv"  # 直播间->设置->功能设置->低电量预警项

    def get_startLiveStreamingIcon(self):
        """
        开启直播
        :return: Element
        """
        return self.get_element(self.startLiveStreamingIcon)

    def get_startLiveStreamingPhotoAlbum(self):
        """
        直播间->已经保存视图文件入口
        :return: Element
        """
        return self.get_element(self.startLiveStreamingPhotoAlbum)

    def get_startLiveStreamingRecord(self):
        """
        直播间->录制视频
        :return: Element
        """
        return self.get_element(self.startLiveStreamingRecord)

    def get_startLiveStreamingPhoto(self):
        """
        直播间->截图
        :return: Element
        """
        return self.get_element(self.startLiveStreamingPhoto)

    def get_startLiveStreamingFullScreen(self):
        """
        直播间->全屏
        :return: Element
        """
        return self.get_element(self.startLiveStreamingFullScreen)

    def get_startLiveStreamingDevShare(self):
        """
        直播间->设备分享
        :return: Element
        """
        return self.get_element(self.startLiveStreamingDevShare)

    def get_startLiveStreamingDevSetting(self):
        """
        直播间->设置
        :return: Element
        """
        return self.get_element(self.startLiveStreamingDevSetting)

    def get_startLiveStreamingBack(self):
        """
        直播间->返回
        :return: Element
        """
        return self.get_element(self.startLiveStreamingBack)

    def get_startLiveStreamingSettingBack(self):
        """
        直播间->设置->返回
        :return: Element
        """
        return self.get_element(self.startLiveStreamingSettingBack)

    def get_startLiveStreamingMotionDetection(self):
        """
        直播间->设置->功能设置->移动侦测
        :return: Element
        """
        return self.get_element(self.startLiveStreamingMotionDetection)

    def get_startLiveStreamingMotionDetectionSwitch(self):
        """
        直播间->设置->功能设置->开关
        :return: Element
        """
        return self.get_element(self.startLiveStreamingMotionDetectionSwitch)

    def get_startLiveStreamingMotionDetectionConfirm(self):
        """
        直播间->设置->功能设置->确定按钮
        :return: Element
        """
        return self.get_element(self.startLiveStreamingMotionDetectionConfirm)

    def get_startLiveStreamingVoiceMonitor(self):
        """
        直播间->设置->功能设置->声压监测
        :return: Element
        """
        return self.get_element(self.startLiveStreamingVoiceMonitor)

    def get_startLiveStreamingObjectRecognition(self):
        """
        直播间->设置->功能设置->物体识别
        :return: Element
        """
        return self.get_element(self.startLiveStreamingObjectRecognition)

    def get_startLiveStreamingCheckAll(self):
        """
        直播间->设置->功能设置->物体识别->全选
        :return: Element
        """
        return self.get_element(self.startLiveStreamingCheckAll)

    def get_startLiveStreamingLowBatteryWarning(self):
        """
        直播间->设置->功能设置->低电量预警
        :return: Element
        """
        return self.get_element(self.startLiveStreamingLowBatteryWarning)

    def get_startLiveStreamingOptions(self):
        """
        直播间->设置->功能设置->低电量预警项
        :return: Element
        """
        return self.get_elements(self.startLiveStreamingOptions)[2]
