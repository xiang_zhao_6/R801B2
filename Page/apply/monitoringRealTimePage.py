"""
    :account for: 动作层
    :modules: 应用-实时侦控
"""
import Base
from selenium.webdriver.common.by import By


class MonitoringRealTimePage(Base.MethodDepot):
    applyButtomIcon = By.ID, "com.zbgd.r801b2:id/ivHome"  # 底部导航栏->应用icon
    monitoringRealTime = By.XPATH, "//*[contains(@text, '实时侦控')]"  # 实时侦控
    monitoringRealTimeDevice = By.ID, "com.zbgd.r801b2:id/iv_video_preview_1"  # 实时侦控->设备封面
    monitoringRealTimeDeviceDtailscChannel = By.ID, "com.zbgd.r801b2:id/channel_num"  # 实时侦控->设备详情->通道
    monitoringRealTimeDeviceDtailscPhoto = By.ID, "com.zbgd.r801b2:id/iv_photo"  # 实时侦控->设备详情->截图
    monitoringRealTimeDeviceDtailscRecord = By.ID, "com.zbgd.r801b2:id/iv_record"  # 实时侦控->设备详情->录制视频
    monitoringRealTimeDeviceDtailscFullScreen = By.ID, "com.zbgd.r801b2:id/iv_fullScreen"  # 实时侦控->设备详情->全屏
    monitoringRealTimeDeviceDtailscFolderExpand0 = By.ID, "com.zbgd.r801b2:id/iv_folder_expand"  # 实时侦控->设备详情->文件夹-视频
    monitoringRealTimeDeviceDtailscFolderExpand1 = By.ID, "com.zbgd.r801b2:id/iv_folder_expand"  # 实时侦控->设备详情->文件夹-图片
    monitoringRealTimeDeviceDtailscBack = By.ID, "com.zbgd.r801b2:id/ic_back"  # 实时侦控->设备详情页面返回
    monitoringRealTimeDeviceListBack = By.ID, "com.zbgd.r801b2:id/iv_back"  # 实时侦控->设备列表页面返回
    monitoringRealTimeDeviceDtailscPhotoAlbum = By.ID, "com.zbgd.r801b2:id/photo_album"  # 实时侦控->设备详情->已经保存视图文件入口
    monitoringRealTimeDeviceDtailscFileName = By.ID, "com.zbgd.r801b2:id/tv_fileName"  # 实时侦控->设备详情->文件夹-视频文件名
    monitoringRealTimeDeviceDtailscRightSelect = By.ID, "com.zbgd.r801b2:id/tvRight"  # 实时侦控->设备详情->文件夹-选择&全选
    monitoringRealTimeDeviceDtailscSend = By.ID, "com.zbgd.r801b2:id/ll_send"  # 实时侦控->设备详情->文件夹-视频发送
    monitoringRealTimeDeviceDtailscDel = By.ID, "com.zbgd.r801b2:id/ll_delete"  # 实时侦控->设备详情->文件夹-视频删除
    monitoringRealTimeDeviceDtailscContent = By.ID, "com.zbgd.r801b2:id/etContent"  # 实时侦控->设备详情->发送-文本框
    monitoringRealTimeDeviceDtailscMemberHead = By.ID, "com.zbgd.r801b2:id/ivHead"  # 实时侦控->设备详情->发送-文本框人员头像
    monitoringRealTimeDeviceDtailscConfirm = By.ID, "com.zbgd.r801b2:id/tvSure"  # 实时侦控->设备详情->发送-确认弹窗(删除)-确定
    monitoringRealTimeDeviceDtailscFileSend = By.ID, "com.zbgd.r801b2:id/message_file_send"  # 实时侦控->设备详情->发送-已发送文件
    monitoringRealTimeDeviceDtailscImgSend = By.ID, "com.zbgd.r801b2:id/message_img_send"  # 实时侦控->设备详情->发送-已发送图片
    monitoringRealTimeDeviceDtailscFileSendError = By.ID, "com.zbgd.r801b2:id/iv_chat_error"  # 实时侦控->设备详情->发送-发送文件失败
    monitoringRealTimeToConsultCancel = By.ID, "com.zbgd.r801b2:id/cancel"  # 实时侦控->取消调阅
    # monitoringRealTime = By.XPATH, "//*[contains(@text, '已选1个文件')]"  # 实时侦控
    monitoringRealTimeDtailscSelectSign = By.XPATH, "//*[contains(@text, '1个文件')]"  # 实时侦控->设备详情->文件夹-选择文件
    monitoringRealTimeToConsult = By.XPATH, "//*[contains(@text, '调阅提示')]"  # 实时侦控->调阅

    def get_applyButtomIcon(self):
        """
        底部导航栏->应用icon
        :return: Element
        """
        return self.get_element(self.applyButtomIcon)

    def get_monitoringRealTime(self):
        """
        实时侦控
        :return: Element
        """
        return self.get_element(self.monitoringRealTime)

    def get_monitoringRealTimeDevice(self):
        """
        实时侦控->设备封面
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDevice)

    def get_monitoringRealTimeDeviceDtailscChannel(self):
        """
        实时侦控->设备详情->通道
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscChannel)

    def get_monitoringRealTimeDeviceDtailscPhoto(self):
        """
        实时侦控->设备详情->截图
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscPhoto)

    def get_monitoringRealTimeDeviceDtailscRecord(self):
        """
        实时侦控->设备详情->录制视频
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscRecord)

    def get_monitoringRealTimeDeviceDtailscFullScreen(self):
        """
        实时侦控->设备详情->全屏
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFullScreen)

    def get_monitoringRealTimeDeviceDtailscFolderExpand0(self):
        """
        实时侦控->设备详情->文件夹-视频
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFolderExpand0)

    def get_monitoringRealTimeDeviceDtailscFolderExpand1(self):
        """
        实时侦控->设备详情->文件夹-图片
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFolderExpand1)

    def get_monitoringRealTimeDeviceDtailscBack(self):
        """
        实时侦控->设备详情页面返回
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscBack)

    def get_monitoringRealTimeDeviceListBack(self):
        """
        实时侦控->设备列表页面返回
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceListBack)

    def get_monitoringRealTimeDeviceDtailscPhotoAlbum(self):
        """
        实时侦控->设备详情->已经保存视图文件入口
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscPhotoAlbum)

    def get_monitoringRealTimeDeviceDtailscFileName(self):
        """
        实时侦控->设备详情->文件夹-视频文件名
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFileName)

    def get_monitoringRealTimeDeviceDtailscRightSelect(self):
        """
        实时侦控->设备详情->文件夹-选择&全选
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscRightSelect)

    def get_monitoringRealTimeDeviceDtailscSend(self):
        """
        实时侦控->设备详情->文件夹-视频发送
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscSend)

    def get_monitoringRealTimeDeviceDtailscDel(self):
        """
        实时侦控->设备详情->文件夹-视频删除
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscDel)

    def get_monitoringRealTimeDeviceDtailscContent(self):
        """
        实时侦控->设备详情->发送-文本框
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscContent)

    def get_monitoringRealTimeDeviceDtailscMemberHead(self):
        """
        实时侦控->设备详情->发送-文本框人员头像
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscMemberHead)

    def get_monitoringRealTimeDeviceDtailscConfirm(self):
        """
        实时侦控->设备详情->发送-确认弹窗(删除)-确定
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscConfirm)

    def get_monitoringRealTimeDeviceDtailscFileSend(self):
        """
        实时侦控->设备详情->发送-已发送文件
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFileSend)

    def get_monitoringRealTimeDeviceDtailscImgSend(self):
        """
        实时侦控->设备详情->发送-已发送图片
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscImgSend)

    def get_monitoringRealTimeDeviceDtailscFileSendError(self):
        """
        实时侦控->设备详情->发送-发送文件失败
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDeviceDtailscFileSendError)

    def get_monitoringRealTimeToConsult(self):
        """
        实时侦控->调阅
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeToConsult)

    def get_monitoringRealTimeToConsultCancel(self):
        """
        实时侦控->取消调阅
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeToConsultCancel)

    def get_monitoringRealTimeDtailscSelectSign(self):
        """
        实时侦控->设备详情->文件夹-选择文件
        :return: Element
        """
        return self.get_element(self.monitoringRealTimeDtailscSelectSign)
