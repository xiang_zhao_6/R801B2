"""
    :account for: 元素层
    :modules: 搜索及聊天
"""
from selenium.webdriver.common.by import By

import Base


class Msg_ChatPage(Base.MethodDepot):
    msgPage_query = By.ID, "com.zbgd.r801b2:id/etContent"
    chat_group_name = By.XPATH, "//*[@text='tempGroupChat']"
    talk_icon = By.ID, "com.zbgd.r801b2:id/talk_listen"
    map_icon = By.ID, "com.zbgd.r801b2:id/talk_map"
    menber_listIcon = By.ID, "com.zbgd.r801b2:id/talk_chat"
    group_info = By.ID, "com.zbgd.r801b2:id/content_view"
    chatPage_cantIcon = By.ID, "com.zbgd.r801b2:id/ll_language"
    chatPage_moreIcon = By.ID, "com.zbgd.r801b2:id/btn_more"
    chatPage_sendwWord = By.ID, "com.zbgd.r801b2:id/et_message"
    chatPage_sendwWordBtn = By.ID, "com.zbgd.r801b2:id/btn_send"
    chatPage_sendwWordBody = By.ID, "com.zbgd.r801b2:id/cv_message_text"
    chat_sendMsgContent = By.XPATH, "//*[contains(@text, '注意')]"
    sendvoice = By.ID, "com.zbgd.r801b2:id/btn_set_mode_voice"
    ass_sendvoice = By.XPATH, "//*[contains(@text, '按住')]"
    longPressToSpeak = By.ID, "com.zbgd.r801b2:id/btn_press_to_speak"
    message_audio = By.ID, "com.zbgd.r801b2:id/message_audio_send_ll"
    chat_error = By.ID, "com.zbgd.r801b2:id/iv_chat_error"
    btn_camera = By.ID, "com.zbgd.r801b2:id/btn_camera"
    camera_start = By.CLASS_NAME, "android.view.View"
    btn_photo = By.ID, "com.zbgd.r801b2:id/btn_photo"
    tvCheck = By.ID, "com.zbgd.r801b2:id/tvCheck"
    sele_photo = By.ID, "com.zbgd.r801b2:id/ps_tv_complete"
    message_img_send = By.ID, "com.zbgd.r801b2:id/message_img_send"
    btn_location = By.ID, "com.zbgd.r801b2:id/btn_location"
    map_send_locasion_query = By.ID, "van-search-1-input"  # 聊天室-更多-发送位置-搜索框
    map_send_locasion_ok = By.XPATH, "//*[contains(@text, '发送')]"
    message_location_send = By.ID, "com.zbgd.r801b2:id/message_location_send"
    btn_group_read = By.ID, "com.zbgd.r801b2:id/btn_group_read"
    iv_photo = By.ID, "com.zbgd.r801b2:id/iv_photo"
    tv_res = By.ID, "com.zbgd.r801b2:id/tv_res"
    le_video_send = By.ID, "com.zbgd.r801b2:id/le_video_send"
    le_read_send = By.ID, "com.zbgd.r801b2:id/le_read_send"
    btn_group_share = By.ID, "com.zbgd.r801b2:id/btn_group_share"
    group_share_check = By.ID, "com.zbgd.r801b2:id/iv_check"
    group_share_btn_next = By.ID, "com.zbgd.r801b2:id/btn_next"
    bar_tvClose = By.ID, "com.zbgd.r801b2:id/tvClose"
    btn_group_tts = By.ID, "com.zbgd.r801b2:id/btn_group_tts"
    btn_group_tts_view_edit = By.ID, "com.zbgd.r801b2:id/view_edit"
    btn_group_tts_view_edit_tv_ok = By.ID, "com.zbgd.r801b2:id/tv_ok"
    chatPageBackBtn = By.ID, "com.zbgd.r801b2:id/ivBack"
    chatPage_radio_layout = By.ID, "com.zbgd.r801b2:id/radio_layout"

    def get_msgPage_query(self):
        """
        消息模块-置顶搜索框
        :return: ele
        """
        return self.get_element(self.msgPage_query)

    def get_chat_group_name(self):
        """
        群聊昵称
        :return: /
        """
        return self.get_element(self.chat_group_name)

    def get_talk_icon(self):
        """
        对讲icon
        :return: ele
        """
        return self.get_element(self.talk_icon)

    def get_map_icon(self):
        """
        地图icon
        :return: /
        """
        return self.get_element(self.map_icon)

    def get_menber_listIcon(self):
        """
        频道在线人员icon
        :return: element
        """
        return self.get_element(self.menber_listIcon)

    def get_group_info(self):
        """
        群列表-群信息
        :return:
        """
        return self.get_element(self.group_info)

    def get_chat_page_cant(self):
        """
        聊天界面-暗语icon
        :return:
        """
        return self.get_element(self.chatPage_cantIcon)

    def get_chatPage_sendwWord(self):
        """
        聊天界面-输入消息
        :return:
        """
        return self.get_element(self.chatPage_sendwWord)

    def get_chatPage_sendwWordBtn(self):
        """
        聊天界面-发送消息
        :return:
        """
        return self.get_element(self.chatPage_sendwWordBtn)

    def get_chatPage_sendwWordBody(self):
        """
        聊天界面-已发送的消息体
        :return:
        """
        return self.get_element(self.chatPage_sendwWordBody)

    def get_chatPage_moreIcon(self):
        """
        聊天界面-更多按钮
        :return:
        """
        return self.get_element(self.chatPage_moreIcon)

    def get_chat_sendMsgContent(self):
        """
        聊天消息内容
        :return:
        """
        return self.get_element(self.chat_sendMsgContent)

    def get_sendvoice(self):
        """
        触发语音按钮
        :return:
        """
        return self.get_element(self.sendvoice)

    def asse_sendvoice(self):
        """
        触发语音按钮,断言占位符
        :return:
        """
        return self.get_element(self.ass_sendvoice)

    def long_press_to_speak(self):
        """
        长按按钮说话
        :return:
        """
        return self.get_element(self.longPressToSpeak)

    def get_message_audio(self):
        """
        发送的语音消息外框
        :return:
        """
        return self.get_element(self.message_audio)

    def get_chat_error(self):
        """
        发送失败的语音消息外框
        :return:
        """
        return self.get_element(self.chat_error)

    def get_btn_camera(self):
        """
        聊天界面-更多Toast:拍摄
        :return:
        """
        return self.get_element(self.btn_camera)

    def get_camera_start(self):
        """
        聊天界面-更多Toast:拍摄界面,点击拍摄
        :return:
        """
        return self.get_element(self.camera_start)

    def get_btn_photo(self):
        """
        聊天界面-更多Toast: 相册
        :return:
        """
        return self.get_element(self.btn_photo)

    def get_tvCheck(self):
        """
        聊天界面-更多Toast: 相册-勾选图片或视频
        :return:
        """
        return self.get_element(self.tvCheck)

    def get_sele_photo(self):
        """
        聊天界面-更多Toast: 相册-勾选图片或视频,点击完成
        :return:
        """
        return self.get_element(self.sele_photo)

    def get_message_img_send(self):
        """
        聊天界面-已发送图片ID
        :return:
        """
        return self.get_element(self.message_img_send)

    def get_btn_location(self):
        """
        聊天界面-更多-发送位置
        :return:
        """
        return self.get_element(self.btn_location)

    def get_map_send_locasion_ok(self):
        """
        聊天界面-更多-发送位置-确定
        :return:
        """
        return self.get_element(self.map_send_locasion_ok)

    def get_message_location_send(self):
        """
        聊天界面-已发送位置
        :return:
        """
        return self.get_element(self.message_location_send)

    def get_btn_group_read(self):
        """
        聊天界面-更多-阅后即焚
        :return:
        """
        return self.get_element(self.btn_group_read)

    def get_iv_photo(self):
        """
        聊天界面-更多-阅后即焚-选择图片
        :return:
        """
        return self.get_element(self.iv_photo)

    def get_tv_res(self):
        """
        聊天界面-更多-阅后即焚-选择图片-从相册选取
        :return:
        """
        return self.get_element(self.tv_res)

    def get_le_video_send(self):
        """
        聊天界面-已发送阅后即焚视频
        :return:
        """
        return self.get_element(self.le_video_send)

    def get_le_read_send(self):
        """
        聊天界面-已发送阅后即焚图片
        :return:
        """
        return self.get_element(self.le_read_send)

    def get_btn_group_share(self):
        """
        聊天界面-更多-设备分享
        :return:
        """
        return self.get_element(self.btn_group_share)

    def get_group_share_check(self):
        """
        聊天界面-更多-设备分享-设备复选框
        :return:
        """
        return self.get_element(self.group_share_check)

    def get_group_share_btn_next(self):
        """
        聊天界面-更多-设备分享-设备复选框-分享
        :return:
        """
        return self.get_element(self.group_share_btn_next)

    def get_bar_tvClose(self):
        """
        聊天界面-更多-阅后即焚-关闭
        :return:
        """
        return self.get_element(self.bar_tvClose)

    def get_btn_group_tts(self):
        """
        聊天界面-更多-应急语音
        :return:
        """
        return self.get_element(self.btn_group_tts)

    def get_btn_group_tts_view_edit(self):
        """
        聊天界面-更多-应急语音-文本输入框
        :return:
        """
        return self.get_element(self.btn_group_tts_view_edit)

    def get_btn_group_tts_view_edit_tv_ok(self):
        """
        聊天界面-更多-应急语音-文本输入框-确定
        :return:
        """
        return self.get_element(self.btn_group_tts_view_edit_tv_ok)

    def get_chatPageBackBtn(self):
        """
        聊天界面-返回
        :return:
        """
        return self.get_element(self.chatPageBackBtn)

    def get_chatPage_radio_layout(self):
        """
        聊天界面-通知栏
        :return:
        """
        return self.get_element(self.chatPage_radio_layout)
