"""
    :account for: 元素层
    :modules: 消息
"""

from selenium.webdriver.common.by import By
import Base


class MessagePage(Base.MethodDepot):
    more_button = By.ID, "com.zbgd.r801b2:id/right_btn_add"
    mb_create_group = By.ID, 'com.zbgd.r801b2:id/start_group'
    mb_sweep = By.ID, 'com.zbgd.r801b2:id/tv_allread'
    mb_set = By.ID, 'com.zbgd.r801b2:id/tv_setting'
    group_temp_type = By.ID, "com.zbgd.r801b2:id/llOne"
    group_temp_check = By.ID, "com.zbgd.r801b2:id/root_layout"
    create_group_step = By.ID, "com.zbgd.r801b2:id/btn_next"
    create_group_nick = By.ID, "com.zbgd.r801b2:id/et_group_name"
    elect_group_owner = By.ID, "com.zbgd.r801b2:id/rlGroupOwen"
    electGroupOwner_save = By.ID, "com.zbgd.r801b2:id/tvRight"
    start_tempGrouopQueryMember = By.ID, "com.zbgd.r801b2:id/et_search"
    tempGroupChat_electMenber_next = By.ID, "com.zbgd.r801b2:id/btn_next"
    case_group_chat = By.ID, "com.zbgd.r801b2:id/llTwo"
    assert_text = By.XPATH, "//*[contains(@text,'请选择群')]"
    assert_electGroup = By.XPATH, "//*[contains(@text,'选择群')]"
    assert_text_group = By.XPATH, "//*[contains(@text,'发起')]"
    assert_title_createGroup = By.XPATH, "//*[contains(@text,'创建')]"
    assert_demo = By.XPATH, "//*[contains(@text,'群聊名称')]"
    averGroupEleTemp = By.XPATH, "//*[contains(@text,'tempG')]"
    averGroupEleCase = By.XPATH, "//*[contains(@text, 'caseG')]"
    assert_msgTitle = By.ID, "com.zbgd.r801b2:id/ivMsg"
    content_container = By.ID, "com.android.permissioncontroller:id/content_container"
    query_window_allow_sudo = By.ID, "com.android.permissioncontroller:id/permission_allow_one_time_button"
    messageModuleQueryBoxCle = By.ID, "com.zbgd.r801b2:id/ivClear"

    def get_more_button(self):
        """
        消息消息界面_更多按钮
        :return: Element
        """
        return self.get_element(self.more_button)

    def get_create_group(self):
        """
        更多_创建群聊
        :return: Element
        """
        return self.get_element(self.mb_create_group)

    def get_mb_sweep(self):
        """
        更多_扫一扫
        :return: Element
        """
        return self.get_element(self.mb_sweep)

    def get_mb_set(self):
        """
        更多_设置
        :return: Element
        """
        return self.get_element(self.mb_set)

    def get_create_group_step(self):
        """
        创建群聊_发起群聊下一步按钮
        :return: Element
        """
        return self.get_element(self.create_group_step)

    def get_group_temp_type(self):
        """
        选择群聊类型_临时群聊
        :return: Element
        """
        return self.get_element(self.group_temp_type)

    def get_group_temp_check(self):
        """
        临时群聊_勾选成员复选框
        :return: Element
        """
        return self.get_elements(self.group_temp_check)

    def get_assert_text(self):
        """
        断言选择群聊类型窗口
        :return: Element
        """
        return self.get_element(self.assert_text)

    def get_assert_text_group(self):
        """
        断言发起群聊页-临时群聊
        :return: Element
        """
        return self.get_element(self.assert_text_group)

    def get_assert_electGroup(self):
        """
        断言临时群聊_选择群主title
        :return: Element
        """
        return self.get_element(self.assert_electGroup)

    def get_assert_title_createGroup(self):
        """
        断言临时群聊_创建群聊Title
        :return: Element
        """
        return self.get_element(self.assert_title_createGroup)

    def get_create_group_nick(self):
        """
        创建临时群聊名称
        :return: Element
        """
        return self.get_element(self.create_group_nick)

    def get_startElectGroupOwner(self):
        """
        创建临时群聊_单击开始选择群主
        :return: Element
        """
        return self.get_element(self.elect_group_owner)

    def get_elect_groupOwner(self):
        """
        创建临时群聊_选择群主
        :return: Element
        """
        return self.get_element(self.group_temp_check)

    def get_electGroupOwner_save(self):
        """
        创建临时群聊_选择群主后、单击保存
        :return: Element
        """
        return self.get_element(self.electGroupOwner_save)

    def get_start_tempGrouopQueryMember(self):
        """
        发起临时群聊-查询群成员
        :return: ele
        """
        return self.get_element(self.start_tempGrouopQueryMember)

    def get_tempGroupChat_electMenber_next(self):
        """
        临时-发起群聊,下一步按钮状态
        :return:
        """
        return self.get_element(self.tempGroupChat_electMenber_next)

    def get_assert_msgTitle(self):
        """
        获取消息模块ID
        :return:
        """
        return self.get_element(self.assert_msgTitle)

    def get_case_group_chat(self):
        """
        选择案件群聊类型_案件群聊
        :return: id
        """
        return self.get_element(self.case_group_chat)

    def get_content_container(self):
        """
        进入app 权限询问弹窗
        :return: id
        """
        return self.get_element(self.content_container)

    def get_query_window_allow_sudo(self):
        """
        进入app 权限询问弹窗-允许本次使用
        :return: id
        """
        return self.get_element(self.query_window_allow_sudo)

    def get_averGroupEleTemp(self):
        """
        消息模块界面-Temp群名
        :return: id
        """
        return self.get_element(self.averGroupEleTemp)

    def get_averGroupEleCase(self):
        """
        消息模块界面-Temp群名
        :return: id
        """
        return self.get_element(self.averGroupEleCase)

    def get_messageModuleQueryBoxCle(self):
        """
        消息模块界面-文本框清除按钮
        :return: id
        """
        return self.get_element(self.messageModuleQueryBoxCle)
