"""
    群聊设置
"""
import Base
from selenium.webdriver.common.by import By


class MsgChatSetPage(Base.MethodDepot):
    group_chat_set = By.ID, "com.zbgd.r801b2:id/ivRight"
    groupChatSetUpTopSwitch = By.ID, "com.zbgd.r801b2:id/upTopSwitch"
    groupChatSetPersonnelAdd = By.ID, "com.zbgd.r801b2:id/ivHead"
    groupChatSetName = By.ID, "com.zbgd.r801b2:id/layGroupName"
    groupChatSetNotice = By.ID, "com.zbgd.r801b2:id/rlNotice"
    groupChatSetPermission = By.ID, "com.zbgd.r801b2:id/rlPermission"
    groupChatSetHistoryMsg = By.ID, "com.zbgd.r801b2:id/layHistoryMsg"
    groupChatSetClearHistoryMsg = By.ID, "com.zbgd.r801b2:id/layClearHistoryMsg"
    groupChatSetDismissGroup = By.ID, "com.zbgd.r801b2:id/btn_dismiss_group"
    groupChatSetAddMemberAddTitle = By.XPATH, "//*[contains(@text, '添加')]"
    groupChatSetMemberLessenTitle = By.XPATH, "//*[contains(@text, '移除')]"
    groupChatSetInRenameNotice = By.XPATH, "//*[contains(@text, '群公告')]"
    groupChatSetRenameNoticeAver = By.XPATH, "//*[contains(@text, '注意')]"
    groupChatSetGroupRename = By.XPATH, "//*[contains(@text, '修改')]"
    groupChatSetPermissionPageTitle = By.XPATH, "//*[contains(@text, '权限')]"
    groupChatSetHistoryMsgTitle = By.XPATH, "//*[contains(@text, '聊天')]"
    groupChatSetMemberIcon = By.ID, "com.zbgd.r801b2:id/iv_icon"
    groupChatSetConfirmBtn = By.ID, "com.zbgd.r801b2:id/btn_next"
    groupChatSetRenameBox = By.ID, "com.zbgd.r801b2:id/etContent"
    groupChatSetRenameNotice = By.ID, "com.zbgd.r801b2:id/etNotice"
    groupChatSetPermissionPageChecked1 = By.ID, "com.zbgd.r801b2:id/rb1"
    groupChatSetPermissionPageChecked2 = By.ID, "com.zbgd.r801b2:id/rb2"
    groupChatSetClearHistoryMsgOk = By.ID, "com.zbgd.r801b2:id/tvSure"

    def get_group_chat_set(self):
        """
        群聊界面-设置按钮
        :return: Element
        """
        return self.get_element(self.group_chat_set)

    def get_groupChatSetUpTopSwitch(self):
        """
        群聊设置界面-置顶按钮
        :return: Element
        """

        return self.get_element(self.groupChatSetUpTopSwitch)

    def get_groupChatSetPersonnelAdd(self):
        """
        群聊设置界面-添加群成员
        :return: Element
        """
        return self.get_elements(self.groupChatSetPersonnelAdd)[-2]

    def get_groupChatSetPersonnelLessen(self):
        """
        群聊设置界面-移除群成员
        :return: Element
        """
        return self.get_elements(self.groupChatSetPersonnelAdd)[-1]

    def get_groupChatSetName(self):
        """
        群聊设置界面-名称
        :return: Element
        """
        return self.get_element(self.groupChatSetName)

    def get_groupChatSetNotice(self):
        """
        群聊设置界面-群公告
        :return: Element
        """
        return self.get_element(self.groupChatSetNotice)

    def get_groupChatSetPermission(self):
        """
        群聊设置界面-权限设置
        :return: Element
        """
        return self.get_element(self.groupChatSetPermission)

    def get_groupChatSetHistoryMsg(self):
        """
        群聊设置界面-查找聊天记录
        :return: Element
        """
        return self.get_element(self.groupChatSetHistoryMsg)

    def get_groupChatSetClearHistoryMsg(self):
        """
        群聊设置界面-清空聊天记录
        :return: Element
        """
        return self.recursion_element(self.groupChatSetClearHistoryMsg)

    def get_groupChatSetDismissGroup(self):
        """
        群聊设置界面-解散群聊
        :return: Element
        """
        return self.recursion_element(self.groupChatSetDismissGroup)

    def get_groupChatSetAddMemberTitle(self):
        """
        群聊设置界面-添加成员界面Title
        :return: Element
        """
        return self.get_element(self.groupChatSetAddMemberAddTitle)

    def get_groupChatSetLessenMemberTitle(self):
        """
        群聊设置界面-移除成员界面Title
        :return: Element
        """
        return self.get_element(self.groupChatSetMemberLessenTitle)

    def get_groupChatSetMemberIcon(self):
        """
        群聊设置界面-添加成员界面-用户头像
        :return: Element
        """
        return self.get_elements(self.groupChatSetMemberIcon)

    def get_groupChatSetConfirmBtn(self):
        """
        群聊设置界面-添加成员界面-确定按钮
        :return: Element
        """
        return self.get_element(self.groupChatSetConfirmBtn)

    def get_groupChatSetRenameBox(self):
        """
        群聊设置界面-修改群名称文本框
        :return: Element
        """
        return self.get_element(self.groupChatSetRenameBox)

    def get_groupChatSetGroupRename(self):
        """
        群聊设置界面-修改群名称界面Title文案
        :return: Element
        """
        return self.get_element(self.groupChatSetGroupRename)

    def get_groupChatSetInRenameNotice(self):
        """
        群聊设置界面-修改群公告界面Title文案
        :return: Element
        """
        return self.get_element(self.groupChatSetInRenameNotice)

    def get_groupChatSetRenameNotice(self):
        """
        群聊设置界面-编辑群公告
        :return: Element
        """
        return self.get_element(self.groupChatSetRenameNotice)

    def get_groupChatSetRenameNoticeAver(self):
        """
        群聊设置界面-编辑保存后的群公告
        :return: Element
        """
        return self.get_element(self.groupChatSetRenameNoticeAver)

    def get_groupChatSetPermissionPageTitle(self):
        """
        群聊设置界面-权限设置界面Title
        :return: Element
        """
        return self.get_element(self.groupChatSetPermissionPageTitle)

    def get_groupChatSetPermissionPageChecked1(self):
        """
        群聊设置界面-权限设置界面一级话权
        :return: Element
        """
        return self.get_element(self.groupChatSetPermissionPageChecked1)

    def get_groupChatSetPermissionPageChecked2(self):
        """
        群聊设置界面-权限设置界面二级话权
        :return: Element
        """
        return self.get_element(self.groupChatSetPermissionPageChecked2)

    def get_groupChatSetHistoryMsgTitle(self):
        """
        群聊设置界面-查找聊天记录界面Title
        :return: Element
        """
        return self.get_element(self.groupChatSetHistoryMsgTitle)

    def get_groupChatSetClearHistoryMsgOk(self):
        """
        群聊设置界面-清空聊天记录-确定按钮
        :return: Element
        """
        return self.get_element(self.groupChatSetClearHistoryMsgOk)
