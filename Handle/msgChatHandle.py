"""
    :account for: 动作层
    :modules: 搜索及聊天
"""
import time

import Page, allure
from appium.webdriver.common.touch_action import TouchAction


class Msg_ChatHandle(Page.Msg_ChatPage):
    @allure.step("置顶搜索框")
    def writeIn_msgPage_query(self, sol):
        self.write_in(self.get_msgPage_query(), sol)
        self.issue_screenshot(self.driver, '查询已创建群聊, 并截图: ')

    @allure.step("固态群主名")
    def click_chat_groupName(self):
        self.hit(self.get_chat_group_name())
        self.issue_screenshot(self.driver, '点击群聊名称, 并截图: ')

    @allure.step("群聊列表-群信息")
    def click_group_info(self):
        self.hit(self.get_group_info())
        self.issue_screenshot(self.driver, '查询已创建群聊,进入聊天界面, 并截图: ')

    @allure.step("群聊界面-输入信息")
    def input_sendwWord(self, words):
        self.write_in(self.get_chatPage_sendwWord(), words)
        self.issue_screenshot(self.driver, '输入信息, 并截图: ')

    @allure.step("群聊界面-发送信息")
    def click_chatPage_sendwWordBtn(self):
        self.hit(self.get_chatPage_sendwWordBtn())
        self.issue_screenshot(self.driver, '发送信息, 并截图: ')

    @allure.step("群聊界面-更多按钮")
    def click_chatPage_moreIcon(self):
        self.hit(self.get_chatPage_moreIcon())
        self.issue_screenshot(self.driver, '点击 群聊界面-更多按钮, 并截图: ')

    @allure.step("点击语音按钮")
    def click_sendvoice(self):
        self.hit(self.get_sendvoice())
        self.issue_screenshot(self.driver, '点击语音按钮, 并截图: ')

    @allure.step("长按语音按钮讲话")
    def click_longPressToSpeak(self):
        self.long_press(self.long_press_to_speak())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '长按语音按钮讲话(10s)后并发出, 并截图: ')

    @allure.step("聊天界面-更多Toast:拍摄")
    def click_btn_camera(self):
        self.hit(self.get_btn_camera())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '点击聊天界面-更多Toast:拍摄, 并截图: ')

    @allure.step("聊天界面-更多Toast:拍摄界面,点击拍摄")
    def click_camera_start(self):
        self.long_press(self.get_camera_start())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多Toast:点击拍摄, 并截图: ')

    @allure.step("聊天界面-更多Toast: 相册")
    def click_btn_photo(self):
        self.hit(self.get_btn_photo())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多Toast: 点击相册, 并截图: ')

    @allure.step("聊天界面-更多Toast: 相册-勾选图片或视频")
    def click_tvCheck(self):
        self.hit(self.get_tvCheck())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多Toast: 相册-勾选图片或视频, 并截图: ')

    @allure.step("聊天界面-更多Toast: 相册-勾选图片或视频,点击完成,发出图片或视频")
    def click_sele_photo(self):
        self.hit(self.get_sele_photo())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多Toast: 相册-勾选图片或视频,点击完成, 并截图: ')

    @allure.step("聊天界面-更多-发送位置")
    def click_btn_location(self):
        self.hit(self.get_btn_location())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-点击发送位置, 并截图: , 并截图: ')

    @allure.step("聊天界面-更多-发送位置-确定")
    def click_map_send_locasion_ok(self):
        self.hit(self.get_map_send_locasion_ok())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-发送位置-点击确定, 并截图: , 并截图: ')

    @allure.step("聊天界面-更多-阅后即焚")
    def write_btn_group_read(self):
        self.hit(self.get_btn_group_read())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-点击阅后即焚, 并截图: , 并截图: ')

    @allure.step("聊天界面-更多-阅后即焚-选择图片")
    def cli_iv_photo(self):
        self.hit(self.get_iv_photo())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-阅后即焚-点击选择图片, 并截图: ')

    @allure.step("聊天界面-更多-阅后即焚-选择图片-从相册选取")
    def cli_tv_res(self):
        self.hit(self.get_tv_res())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-阅后即焚-选择图片-从相册选取, 并截图: ')

    @allure.step("聊天界面-更多-设备分享")
    def cli_btn_group_share(self):
        self.hit(self.get_btn_group_share())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-点击设备分享, 并截图: ')

    @allure.step("聊天界面-更多-设备分享-设备复选框")
    def cli_group_share_check(self):
        self.hit(self.get_group_share_check())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-设备分享-点击勾选设备复选框: , 并截图: ')

    @allure.step("聊天界面-更多-设备分享-设备复选框-分享")
    def cli_group_share_btn_next(self):
        self.hit(self.get_group_share_btn_next())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-设备分享-设备复选框-点击分享: , 并截图: ')

    @allure.step("聊天界面-更多-阅后即焚-关闭")
    def cli_bar_tvClose(self):
        self.hit(self.get_bar_tvClose())
        time.sleep(0.5)
        self.issue_screenshot(self.driver, '聊天界面-更多-阅后即焚,点击关闭: , 并截图: ')

    @allure.step(" 聊天界面-更多-应急语音")
    def click_btn_group_tts(self):
        self.hit(self.get_btn_group_tts())
        self.issue_screenshot(self.driver, ' 聊天界面-更多-应急语音, 并截图: ')

    @allure.step("聊天界面-更多-应急语音-文本输入框")
    def input_btn_group_tts_view_edit(self, view_edit_content):
        self.write_in(self.get_btn_group_tts_view_edit(), view_edit_content)
        self.issue_screenshot(self.driver, ' 聊天界面-更多-应急语音-文本输入框输入文字, 并截图: ')

    @allure.step("聊天界面-更多-应急语音-文本输入框-确定")
    def click_btn_group_tts_view_edit_tv_ok(self):
        self.hit(self.get_btn_group_tts_view_edit_tv_ok())
        self.issue_screenshot(self.driver, ' 聊天界面-更多-应急语音-文本输入框-确定, 并截图: ')

    @allure.step("聊天界面-返回")
    def click_chatPageBackBtn(self):
        self.hit(self.get_chatPageBackBtn())
        self.issue_screenshot(self.driver, ' 聊天界面-返回, 并截图: ')
