import Handle


class HandleTotal():
    def __init__(self, driver):
        self.driver = driver

    @property
    def init_index_handle(self):
        """
        应用锁页面
        :return: /
        """
        return Handle.IndexHandler(self.driver)

    @property
    def init_infor_handle(self):
        """
        消息模块
        :return:/
        """
        return Handle.MessageHandle(self.driver)

    @property
    def ini_msgChatHandle(self):
        """
        消息模块_聊天
        :return: /
        """
        return Handle.Msg_ChatHandle(self.driver)

    @property
    def init_msgChatSetHandle(self):
        """
        消息模块_聊天
        :return: /
        """
        return Handle.MsgChatSetHandle(self.driver)

    @property
    def init_contactList(self):
        """
        通讯录
        """
        return Handle.ContactListHandle(self.driver)

    @property
    def init_monitoringRealTime(self):
        """
        应用-实时侦控
        """
        return Handle.MonitoringRealTimeHandle(self.driver)

    @property
    def init_emergencyAlarmHandle(self):
        """
        紧急报警
        """
        return Handle.EmergencyAlarmHandle(self.driver)

    @property
    def init_startLiveStreamingHandle(self):
        """
        紧急报警
        """
        return Handle.apply.StartLiveStreamingHandle(self.driver)
