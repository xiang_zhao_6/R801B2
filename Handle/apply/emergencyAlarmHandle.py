"""
    :account for: 动作层
    :modules: 应用-应用-紧急报警
"""
import Page.apply.emergencyAlarmPage
import allure
from Page.apply.emergencyAlarmPage import EmergencyAlarmPage


class EmergencyAlarmHandle(Page.EmergencyAlarmPage):
    @allure.step("紧急报警Icon")
    def click_emergencyAlarmIcon(self):
        self.hit(self.get_emergencyAlarmSwitch())
        self.issue_screenshot(self.driver, '点击紧急报警Icon, 并截图: ')
