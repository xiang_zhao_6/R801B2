"""
    :account for: 动作层
    :modules: 应用-实时侦控
"""
import Page.apply.monitoringRealTimePage
from Page.apply.monitoringRealTimePage import MonitoringRealTimePage
import Page, allure


class MonitoringRealTimeHandle(Page.MonitoringRealTimePage):
    @allure.step("底部导航栏->应用icon")
    def click_applyButtomIcon(self):
        self.hit(self.get_applyButtomIcon())
        self.issue_screenshot(self.driver, '点击底部导航栏->应用icon, 并截图: ')

    @allure.step("实时侦控")
    def click_monitoringRealTime(self):
        self.hit(self.get_monitoringRealTime())
        self.issue_screenshot(self.driver, '点击实时侦控icon, 并截图: ')

    @allure.step("实时侦控->设备封面")
    def click_monitoringRealTimeDevice(self):
        self.hit(self.get_monitoringRealTimeDevice())
        self.issue_screenshot(self.driver, '点击实时侦控->设备封面, 并截图: ')

    @allure.step("实时侦控->设备详情->通道")
    def click_monitoringRealTimeDeviceDtailscChannel(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscChannel())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->点击通道, 并截图: ')

    @allure.step("实时侦控->设备详情->截图")
    def click_monitoringRealTimeDeviceDtailscPhoto(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscPhoto())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->点击截图, 并截图: ')

    @allure.step("实时侦控->设备详情->录制视频")
    def click_monitoringRealTimeDeviceDtailscRecord(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscRecord())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->点击录制视频, 并截图: ')

    @allure.step("实时侦控->设备详情->全屏")
    def click_monitoringRealTimeDeviceDtailscFullScreen(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscFullScreen())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->点击全屏, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-视频")
    def click_monitoringRealTimeDeviceDtailscFolderExpand0(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscFolderExpand0())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击视频, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-图片")
    def click_monitoringRealTimeDeviceDtailscFolderExpand1(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscFolderExpand1())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击图片, 并截图: ')

    @allure.step("实时侦控->设备详情页面返回")
    def click_monitoringRealTimeDeviceDtailscBack(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscBack())
        self.issue_screenshot(self.driver, '实时侦控->设备详情页面点击返回, 并截图: ')

    @allure.step("实时侦控->设备列表页面返回")
    def click_monitoringRealTimeDeviceListBack(self):
        self.hit(self.get_monitoringRealTimeDeviceListBack())
        self.issue_screenshot(self.driver, '实时侦控->设备列表页面点击返回, 并截图: ')

    @allure.step("实时侦控->设备详情->已经保存视图文件入口")
    def click_monitoringRealTimeDeviceDtailscPhotoAlbum(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscPhotoAlbum())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->进入视图文件入口, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-视频文件名")
    def click_monitoringRealTimeDeviceDtailscFileName(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscFileName())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击视频文件名, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-选择&全选")
    def click_monitoringRealTimeDeviceDtailscRightSelect(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscRightSelect())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击选择&全选, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-视频发送")
    def click_monitoringRealTimeDeviceDtailscSend(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscSend())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击发送视频, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-视频删除")
    def click_monitoringRealTimeDeviceDtailscDel(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscDel())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击删除视频, 并截图: ')

    @allure.step("实时侦控->设备详情->发送-文本框")
    def click_monitoringRealTimeDeviceDtailscContent(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscContent())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->发送-文本框, 并截图: ')

    @allure.step("实时侦控->设备详情->发送-文本框人员头像")
    def click_monitoringRealTimeDeviceDtailscMemberHead(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscMemberHead())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->发送-点击文本框人员头像, 并截图: ')

    @allure.step("实时侦控->设备详情->发送-确认弹窗(删除)-确定")
    def click_monitoringRealTimeDeviceDtailscConfirm(self):
        self.hit(self.get_monitoringRealTimeDeviceDtailscConfirm())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->发送-确认弹窗(删除)-点击确定, 并截图: ')

    @allure.step("实时侦控->设备详情->文件夹-选择文件")
    def click_monitoringRealTimeDtailscSelectSign(self):
        self.hit(self.get_monitoringRealTimeDtailscSelectSign())
        self.issue_screenshot(self.driver, '实时侦控->设备详情->文件夹-点击选择文件, 并截图: ')

    @allure.step("实时侦控->取消调阅")
    def click_monitoringRealTimeToConsultCancel(self):
        self.hit(self.get_monitoringRealTimeToConsultCancel())
        self.issue_screenshot(self.driver, '实时侦控->点击取消调阅, 并截图: ')
