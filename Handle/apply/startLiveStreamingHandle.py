"""
    :account for: 动作层
    :modules: 应用-开启直播
"""
import allure

import Page


class StartLiveStreamingHandle(Page.StartLiveStreamingPage):
    @allure.step("开启直播Icon")
    def click_startLiveStreamingIcon(self):
        self.hit(self.get_startLiveStreamingIcon())
        self.issue_screenshot(self.driver, '点击开启直播Icon, 并截图: ')

    @allure.step("直播间->已经保存视图文件入口")
    def click_startLiveStreamingPhotoAlbum(self):
        self.hit(self.get_startLiveStreamingPhotoAlbum())
        self.issue_screenshot(self.driver, '直播间->进入已经保存视图文件入口, 并截图: ')

    @allure.step("直播间->录制视频")
    def click_startLiveStreamingRecord(self):
        self.hit(self.get_startLiveStreamingRecord())
        self.issue_screenshot(self.driver, '直播间->点击录制视频, 并截图: ')

    @allure.step("直播间->截图")
    def click_startLiveStreamingPhoto(self):
        self.hit(self.get_startLiveStreamingPhoto())
        self.issue_screenshot(self.driver, '直播间->点击截图, 并截图: ')

    @allure.step("直播间->全屏")
    def click_startLiveStreamingFullScreen(self):
        self.hit(self.get_startLiveStreamingFullScreen())
        self.issue_screenshot(self.driver, '直播间->点击全屏, 并截图: ')

    @allure.step("直播间->设备分享")
    def click_startLiveStreamingDevShare(self):
        self.hit(self.get_startLiveStreamingDevShare())
        self.issue_screenshot(self.driver, '直播间->设备分享, 并截图: ')

    @allure.step("直播间->设置")
    def click_startLiveStreamingDevSetting(self):
        self.hit(self.get_startLiveStreamingDevSetting())
        self.issue_screenshot(self.driver, '直播间->设置, 并截图: ')

    @allure.step("直播间->返回")
    def click_startLiveStreamingBack(self):
        self.hit(self.get_startLiveStreamingBack())
        self.issue_screenshot(self.driver, '直播间->点击返回, 并截图: ')

    @allure.step("直播间->设置->返回")
    def click_startLiveStreamingSettingBack(self):
        self.hit(self.get_startLiveStreamingSettingBack())
        self.issue_screenshot(self.driver, '直播间->设置->点击返回, 并截图: ')

    @allure.step("直播间->设置->功能设置->移动侦测")
    def click_startLiveStreamingMotionDetection(self):
        self.hit(self.get_startLiveStreamingMotionDetection())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->移动侦测, 并截图: ')

    @allure.step("直播间->设置->功能设置->开关")
    def click_startLiveStreamingMotionDetectionSwitch(self):
        self.hit(self.get_startLiveStreamingMotionDetectionSwitch())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->开关, 并截图: ')

    @allure.step("直播间->设置->功能设置->确定按钮")
    def click_startLiveStreamingMotionDetectionConfirm(self):
        self.hit(self.get_startLiveStreamingMotionDetectionConfirm())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->点击确定按钮, 并截图: ')

    @allure.step("直播间->设置->功能设置->声压监测")
    def click_startLiveStreamingVoiceMonitor(self):
        self.hit(self.get_startLiveStreamingVoiceMonitor())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->点击声压监测, 并截图: ')

    @allure.step("直播间->设置->功能设置->物体识别")
    def click_startLiveStreamingObjectRecognition(self):
        self.hit(self.get_startLiveStreamingObjectRecognition())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->点击物体识别, 并截图: ')

    @allure.step("直播间->设置->功能设置->物体识别->全选")
    def click_startLiveStreamingCheckAll(self):
        self.hit(self.get_startLiveStreamingCheckAll())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->物体识别->点击全选, 并截图: ')

    @allure.step("直播间->设置->功能设置->低电量预警")
    def click_startLiveStreamingLowBatteryWarning(self):
        self.hit(self.get_startLiveStreamingLowBatteryWarning())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->低电量预警, 并截图: ')

    @allure.step("直播间->设置->功能设置->低电量预警项")
    def click_startLiveStreamingOptions(self):
        self.hit(self.get_startLiveStreamingOptions())
        self.issue_screenshot(self.driver, '直播间->设置->功能设置->低电量预警项, 并截图: ')
