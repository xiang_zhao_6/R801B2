"""
    :account for: 动作层
    :modules: 应用锁
"""
import allure
import Page
from log.configLog import Logger


class IndexHandler(Page.IndexPage):
    @allure.step("单击切换服务器")
    def click_index_page_element(self):
        self.hit(self.get_lock_screen_server())
        self.issue_screenshot(self.driver, '单击锁屏页_切换服务器, 并截图: ')

    @allure.step("输入服务器地址")
    def write_ip(self, ip):
        self.write_in(self.get_input_server(), ip)
        self.issue_screenshot(self.driver, '输入服务器地址, 并截图: ')

    @allure.step("输入端口")
    def write_port(self, port):
        self.write_in(self.get_input_port(), port)
        self.issue_screenshot(self.driver, '输入端口: ')

    @allure.step("单击保存")
    def click_but(self):
        self.hit(self.get_input_but())
        self.issue_screenshot(self.driver, '单击保存: ')

    @allure.step("输入应用锁键盘密码")
    def cl_keypad(self, appLockPwd):
        self.write_in(self.get_keypad_five(), appLockPwd)
        self.issue_screenshot(self.driver, '输入应用锁键盘密码: ')
