"""
    :account for: 动作层
    :modules: 群聊设置
"""
import time

import Page, allure
from appium.webdriver.common.touch_action import TouchAction


class MsgChatSetHandle(Page.MsgChatSetPage):
    @allure.step("群聊界面-进入群设置")
    def click_msgChatSetBtn(self):
        self.hit(self.get_group_chat_set())
        self.issue_screenshot(self.driver, '群聊界面-进入群设置, 并截图: ')

    @allure.step("群聊界面-置顶按钮")
    def click_groupChatSetUpTopSwitch(self):
        self.hit(self.get_groupChatSetUpTopSwitch())
        self.issue_screenshot(self.driver, '群聊界面-点击置顶按钮, 并截图: ')

    @allure.step("群聊设置界面-添加群成员")
    def click_groupChatSetPersonnelAdd(self):
        self.hit(self.get_groupChatSetPersonnelAdd())
        self.issue_screenshot(self.driver, '群聊设置界面-添加群成员, 并截图: ')

    @allure.step("群聊设置界面-添加成员界面-用户头像")
    def click_groupChatSetPersonnelAdd(self):
        self.hit(self.get_groupChatSetPersonnelAdd())
        self.issue_screenshot(self.driver, '群聊设置界面-添加成员界面-用户头像, 并截图: ')

    @allure.step("群聊设置界面-添加成员界面-确定按钮")
    def click_groupChatSetConfirmBtn(self):
        self.hit(self.get_groupChatSetConfirmBtn())
        self.issue_screenshot(self.driver, '群聊设置界面-添加成员界面-确定按钮, 并截图: ')

    @allure.step("群聊设置界面-名称")
    def click_groupChatSetName(self):
        self.hit(self.get_groupChatSetName())
        self.issue_screenshot(self.driver, '群聊设置界面-名称, 并截图: ')

    @allure.step("群聊设置界面-移除群成员")
    def click_groupChatSetPersonnelLessen(self):
        self.hit(self.get_groupChatSetPersonnelLessen())
        self.issue_screenshot(self.driver, '群聊设置界面-移除群成员, 并截图: ')

    @allure.step("群聊设置界面-修改群名称文本框")
    def input_groupChatSetRenameBox(self, content):
        self.write_in(self.get_groupChatSetRenameBox(), content)
        self.issue_screenshot(self.driver, '群聊设置界面-修改群名称文本框, 并截图: ')

    @allure.step("群聊设置界面-群公告")
    def click_groupChatSetNotice(self):
        self.hit(self.get_groupChatSetNotice())
        self.issue_screenshot(self.driver, '群聊设置界面-进入群公告, 并截图: ')

    @allure.step("群聊设置界面-编辑群公告")
    def write_groupChatSetRenameNotice(self, text):
        self.write_in(self.get_groupChatSetRenameNotice(), text)
        self.issue_screenshot(self.driver, '群聊设置界面-编辑群公告, 并截图: ')

    @allure.step("群聊设置界面-权限设置界面一级话权")
    def click_groupChatSetPermissionPageChecked1(self):
        self.hit(self.get_groupChatSetPermissionPageChecked1())
        self.issue_screenshot(self.driver, '群聊设置界面-权限设置界面一级话权, 并截图: ')

    @allure.step("群聊设置界面-权限设置界面二级话权")
    def click_groupChatSetPermissionPageChecked2(self):
        self.hit(self.get_groupChatSetPermissionPageChecked2())
        self.issue_screenshot(self.driver, '群聊设置界面-权限设置界面二级话权, 并截图: ')

    @allure.step("群聊设置界面-权限设置")
    def click_groupChatSetPermission(self):
        self.hit(self.get_groupChatSetPermission())
        self.issue_screenshot(self.driver, '进入群聊设置界面-权限设置, 并截图: ')

    @allure.step("群聊设置界面-查找聊天记录")
    def click_groupChatSetHistoryMsg(self):
        self.hit(self.get_groupChatSetHistoryMsg())
        self.issue_screenshot(self.driver, '进入群聊设置界面-查找聊天记录页面, 并截图: ')

    @allure.step("群聊设置界面-清空聊天记录")
    def click_groupChatSetClearHistoryMsg(self):
        self.hit(self.get_groupChatSetClearHistoryMsg())
        self.issue_screenshot(self.driver, '群聊设置界面-清空聊天记录, 并截图: ')

    @allure.step("群聊设置界面-清空聊天记录-确定按钮")
    def click_groupChatSetClearHistoryMsgOk(self):
        self.hit(self.get_groupChatSetClearHistoryMsgOk())
        self.issue_screenshot(self.driver, '群聊设置界面-清空聊天记录-确定按钮, 并截图: ')

    @allure.step("群聊设置界面-解散群聊")
    def click_groupChatSetDismissGroup(self):
        self.hit(self.get_groupChatSetDismissGroup())
        self.issue_screenshot(self.driver, '群聊设置界面-解散群聊, 并截图: ')
