"""
    :account for: 动作层
    :modules: 消息
"""

import time
import allure
from selenium.common.exceptions import NoSuchElementException

import Page


class MessageHandle(Page.MessagePage):
    @allure.step("消息界面_点击更多按钮")
    def click_more_button(self):
        self.hit(self.get_more_button())
        self.issue_screenshot(self.driver, '点击更多按钮, 并截图: ')

    @allure.step("更多_创建群聊")
    def click_create_group(self):
        self.hit(self.get_create_group())
        self.issue_screenshot(self.driver, '点击创建群聊按钮, 并截图: ')

    @allure.step("选择群聊类型_临时群聊")
    def click_group_temp_type(self):
        self.hit(self.get_group_temp_type())
        self.issue_screenshot(self.driver, '选择群聊类型_临时群聊, 并截图: ')

    @allure.step("临时群聊-发起群聊,勾选成员")
    def click_group_temp_check(self):
        for test in self.get_group_temp_check():
            test.click()
        self.issue_screenshot(self.driver, '临时群聊,勾选成员复选框, 并截图: ')

    @allure.step("更多_扫一扫")
    def click_mb_sweep(self):
        self.hit(self.get_mb_sweep())
        self.issue_screenshot(self.driver, '点击扫一扫按钮, 并截图: ')

    @allure.step("更多_设置")
    def click_mb_set(self):
        self.hit(self.get_mb_set())
        self.issue_screenshot(self.driver, '点击设置按钮, 并截图: ')

    @allure.step("发起群聊_下一步按钮")
    def click_create_group_step(self):
        self.hit(self.get_create_group_step())
        self.issue_screenshot(self.driver, '点击下一步按钮, 并截图: ')

    @allure.step("创建临时群聊_输入名称")
    def write_create_group_nick(self, nick):
        self.write_in(self.get_create_group_nick(), nick)
        self.issue_screenshot(self.driver, '输入名称, 并截图: ')

    @allure.step("创建案件群聊_输入名称")
    def write_create_fixedGroupNick(self, caseGroupChat):
        self.write_in(self.get_create_group_nick(), caseGroupChat)
        self.issue_screenshot(self.driver, '输入名称, 并截图: ')

    @allure.step("创建临时群聊_单击开始选择群主")
    def click_startElectGroupOwner(self):
        self.hit(self.get_startElectGroupOwner())
        self.issue_screenshot(self.driver, '临时群聊_单击开始选择群主, 并截图: ')

    @allure.step("创建临时群聊_选择群主")
    def click_elect_groupOwner(self):
        self.hit(self.get_elect_groupOwner())
        self.issue_screenshot(self.driver, '选择群主, 并截图: ')

    @allure.step("创建临时群聊界面、单击保存")
    def click_electGroupOwner_save(self):
        self.hit(self.get_electGroupOwner_save())
        self.issue_screenshot(self.driver, '创建临时界面、单击保存, 并截图: ')

    @allure.step("保存临时群聊")
    def reuse_click_createSave(self):
        self.hit(self.get_electGroupOwner_save())
        self.issue_screenshot(self.driver, '临时群聊,点击保存, 并截图: ')

    @allure.step("发起群聊-查询群成员")
    def click_start_tempGrouopQueryMember(self, member):
        self.write_in(self.get_start_tempGrouopQueryMember(), member)
        self.issue_screenshot(self.driver, '临时群聊-发起临时群聊-查询群成员, 并截图: ')

    @allure.step("选择案件群聊")
    def click_case_group_chat(self):
        self.hit(self.get_case_group_chat())
        self.issue_screenshot(self.driver, '选择案件群聊_案件群聊, 并截图: ')

    @allure.step("进入app 权限询问弹窗-允许本次使用")
    def click_query_window_allow_sudo(self):
        self.hit(self.get_query_window_allow_sudo())
        self.issue_screenshot(self.driver, '进入app 权限询问弹窗-点击允许本次使用, 并截图: ')

    @allure.step("消息模块界面-文本框清除按钮")
    def click_messageModuleQueryBoxCle(self):
        self.hit(self.get_messageModuleQueryBoxCle())
        self.issue_screenshot(self.driver, '消息模块界面-点击->文本框清除按钮, 并截图: ')
