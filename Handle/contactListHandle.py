"""
    :account for: 动作层
    :modules: 通讯录
"""
import allure
import Page
from log.configLog import Logger


class ContactListHandle(Page.ContactListPage, Page.MemberChitchatPage):
    @allure.step("单击底部通讯录icon")
    def click_bottomNavContactListIcon(self):
        self.hit(self.get_bottomNavContactListIcon())
        self.issue_screenshot(self.driver, '单击底部通讯录icon, 并截图: ')

    @allure.step("置顶搜索框")
    def write_contactListSearchBox(self, content):
        self.write_in(self.get_contactListSearchBox(), content)
        self.issue_screenshot(self.driver, '置顶搜索框,查询当前账号名称, 并截图: ')

    @allure.step("人员头像")
    def click_contactListMemberIcon(self):
        self.hit(self.get_contactListMemberIcon())
        self.issue_screenshot(self.driver, '通讯录成员列表,人员头像, 并截图: ')

    @allure.step("人员详情页->发消息")
    def click_contactListSendMsg(self):
        self.hit(self.get_contactListSendMsg())
        self.issue_screenshot(self.driver, '人员详情页->发消息, 并截图: ')

    @allure.step("成员聊天界面->消息框")
    def write_memberChitchatBottomMsgTextBox(self, context):
        self.write_in(self.get_memberChitchatBottomMsgTextBox(), context)
        self.issue_screenshot(self.driver, '成员聊天界面->消息框, 并截图: ')

    @allure.step("成员聊天界面->发送消息按钮")
    def click_memberChitchatDetails_btn_send(self):
        self.hit(self.get_memberChitchatDetails_btn_send())
        self.issue_screenshot(self.driver, '成员聊天界面->发送消息按钮, 并截图: ')

    @allure.step("成员聊天界面->更多按钮")
    def click_memberChitchatBottomMoreBtn(self):
        self.hit(self.get_memberChitchatBottomMoreBtn())
        self.issue_screenshot(self.driver, '成员聊天界面->更多按钮, 并截图: ')

    @allure.step("成员聊天界面->一键清除")
    def click_memberChitchat_ll_clear(self):
        self.hit(self.get_memberChitchatDetails_ll_clear())
        self.issue_screenshot(self.driver, '成员聊天界面->点击一键清除, 并截图: ')

    @allure.step("成员聊天界面->一键清除->确认按钮")
    def click_memberChitchat_tv_ok(self):
        self.hit(self.get_memberChitchatDetails_tv_ok())
        self.issue_screenshot(self.driver, '成员聊天界面->一键清除->确认按钮, 并截图: ')

    @allure.step("成员聊天界面->语音按钮")
    def click_memberChitchatBottomVoiceBtn(self):
        self.hit(self.get_memberChitchatBottomVoiceBtn())
        self.issue_screenshot(self.driver, '成员聊天界面->点击语音按钮, 并截图: ')

    @allure.step("成员聊天界面->按住说话")
    def click_memberChitchatBottomPressToSpeak(self):
        self.long_press(self.get_memberChitchatBottomPressToSpeak())
        self.issue_screenshot(self.driver, '成员聊天界面->常按住说话, 并截图: ')

    @allure.step("成员聊天界面->更多->相册")
    def click_memberChitchatMorePhoto(self):
        self.hit(self.get_memberChitchatMorePhoto())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击相册, 并截图: ')

    @allure.step("成员聊天界面->更多->相册->复选框")
    def click_memberChitchatMorePhotoCheck(self):
        self.hit(self.get_memberChitchatMorePhotoCheck())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->相册->点击复选框, 并截图: ')

    @allure.step("成员聊天界面->更多->相册->复选框->已完成")
    def click_memberChitchatMorePhotoCheckCompleteSelect(self):
        self.hit(self.get_memberChitchatMorePhotoCheckCompleteSelect())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->相册->复选框->点击已完成, 并截图: ')

    @allure.step("返回按钮")
    def click_memberChitchatTopBack(self):
        self.hit(self.get_memberChitchatTopBack())
        self.issue_screenshot(self.driver, '点击返回按钮, 并截图: ')

    @allure.step("成员聊天界面->更多->相册->返回")
    def click_memberChitchatLeftBack(self):
        self.hit(self.get_memberChitchatMorePhotoCheckLeftBack())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->相册->点击返回按钮, 并截图: ')

    @allure.step("成员聊天界面->更多->发送位置")
    def click_memberChitchatLocation(self):
        self.hit(self.get_memberChitchatLocation())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击发送位置, 并截图: ')

    @allure.step("位置界面->发送按钮")
    def click_memberChitchatLocationSendBtn(self):
        self.hit(self.get_memberChitchatLocationSendBtn())
        self.issue_screenshot(self.driver, '位置界面->点击发送位置, 并截图: ')

    @allure.step("成员聊天界面->更多->音频通话")
    def click_memberChitchatAudioCall(self):
        self.hit(self.get_memberChitchatAudioCall())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击音频通话, 并截图: ')

    @allure.step("成员聊天界面->更多->音频通话->挂断")
    def click_memberChitchatCancel(self):
        self.hit(self.get_memberChitchatCancel())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击音频通话->挂断, 并截图: ')

    @allure.step("成员聊天界面->更多->视频通话->挂断电话")
    def click_memberChitchatVideoCallCancel(self):
        self.hit(self.get_memberChitchatVideoCallCancel())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->视频通话->点击挂断电话, 并截图: ')

    @allure.step("成员聊天界面->更多->视频通话")
    def click_memberChitchatVideoCall(self):
        self.hit(self.get_memberChitchatVideoCall())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击视频通话, 并截图: ')

    @allure.step("成员聊天界面->更多->阅后即焚")
    def click_memberChitchatUserRead(self):
        self.hit(self.get_memberChitchatUserRead())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击阅后即焚, 并截图: ')

    @allure.step("成员聊天界面->更多->设备分享")
    def click_memberChitchatUserShare(self):
        self.hit(self.get_memberChitchatUserShare())
        self.issue_screenshot(self.driver, '成员聊天界面->更多->点击设备分享, 并截图: ')

    @allure.step("成员聊天界面->阅后即焚->关闭")
    def click_memberChitchatBurnAfterReadingClose(self):
        self.hit(self.get_memberChitchatBurnAfterReadingClose())
        self.issue_screenshot(self.driver, '成员聊天界面->阅后即焚->点击关闭, 并截图: ')

    @allure.step("设备分享页,设备元素列表->复选框")
    def click_memberChitchatDeviceShareListCheck(self):
        self.hit(self.get_memberChitchatDeviceShareListCheck())
        self.issue_screenshot(self.driver, '设备分享页,设备元素列表->点击复选框, 并截图: ')

    @allure.step("设备分享页,设备元素列表->分享")
    def click_memberChitchatDeviceShareBtn(self):
        self.hit(self.get_memberChitchatDeviceShareBtn())
        self.issue_screenshot(self.driver, '设备分享页,设备元素列表->点击分享, 并截图: ')
